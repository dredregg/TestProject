var connect = require('connect');
var __ = require('underscore');
var http = require('http');
var qs = require('querystring');
var dbMarches = require('./dbSqlFunctions/marches');
var dbPlayerInformation = require('./dbSqlFunctions/playerInformation');
var dbMatrix = require('./dbSqlFunctions/matrix');

var cityUnit = require('./dbSqlFunctions/cityUnit');
var cityResource = require('./dbSqlFunctions/cityResource');
var ranncelUpdate = require('./dbSqlFunctions/cel/ranncelSettingsUpdate');
var raymondTestTable = require('./dbSqlFunctions/raymond/testtable');
var celShop = require('./dbSqlFunctions/cel/shop');

var app = connect();

dbMatrix.initializeMatrix(initializeGameServer);
var testTable = require('./dbSqlFunctions/raymond/testtable');

var battlestate = require('./battlestate');
var battleStateInstance = battlestate();

var troops = require('./troops');

var rainLocalChat = require('./dbSqlFunctions/rain/rainChatAliianceConnection');


/* ---------------------- newly added ron --------------------- */
var rs = require('./dbSqlFunctions/ron/Report/ReportSystem');
var rr = require('./dbSqlFunctions/ron/Resource/ReduceResourceSystem');
var ru = require('./dbSqlFunctions/ron/Unit/ReduceUnitSystem');
var rcs = require('./dbSqlFunctions/ron/CityStats/ReduceCityStatSystem');
var dcs = require('./dbSqlFunctions/ron/CityStats/DestroyCitySystem');

var dbCityStats = require('./dbSqlFunctions/ron/CityStats/dbCityStats');

var ReportItem = require('./dbSqlFunctions/ron/Report/ReportItem');
var dbReportScoutUnit = require('./dbSqlFunctions/ron/Report/dbReportScoutUnit');
var worldMap = require('./dbSqlFunctions/ron/Worldmap/Worldmap');
/* ---------------------- end newly added ron --------------------- */

//Raymond var

var testTable = require('./dbSqlFunctions/raymond/testtable');
var AlliancesJs = require('./dbSqlFunctions/raymond/Alliances');

//end of raymond codes

/*-----------rain-----------*/
var rainLocalChat = require('./dbSqlFunctions/rain/rainChatAliianceConnection');
var rainMarketModule = require('./dbSqlFunctions/rain/rainMarketConnection');

/*-----------rain end-----------*/


/* -------------- Initialization Scripts ------------------------ */

function initializeGameServer(startGameServer) {
    console.log("INITIALIZATION WAS CALLED");
    if (startGameServer) {
        http.createServer(app).listen(1228);
        console.log('Server is running..');

    }

}

/* -------------- End Initialization Script --------------------- */

function computeTimeArrival(distance, speed) {
    return (distance * 100) / speed * 60;
    console.log((distance * 100) / speed * 60);//in min
}

function getAllCloseCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            dbPlayerInformation.getAllCloseCity(post.city_id, post.xpos, post.ypos, function (err, result)

            {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                var cities = [];
                __.each(result, function (element, index, list) {
                    cities.push({
                        userFullName: element.user_full_name, xpos: element.xpos,
                        ypos: element.ypos, cityName: element.title, distance: element.distance,
                        targetCityId: element.cityno
                    });
                });
                response.write(JSON.stringify({cities: cities, rawCity: result}));
                response.end();
            });
        });

    }
}

/** newly added function **/
function getAllCloseBarbarianCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            worldMap.getAllCloseBarbarianCity(post.xpos, post.ypos, function (err, result)
            {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({barbarianCity: result}));
                response.end();
            });
        });

    }
}

function getCloseAllFields(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            worldMap.getCloseAllFields(post.userId, post.xpos, post.ypos, function (err, result)
            {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fieldCity: result}));
                response.end();
            });
        });

    }
}

function getClosePlayerCities(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            worldMap.getClosePlayersCity(post.userId, post.xpos, post.ypos, function (err, result)
            {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({playerCity: result}));
                response.end();
            });
        });

    }
}

function getCloseEmptyLandCities(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            worldMap.getCloseEmptyLandCity(post.userId, post.xpos, post.ypos, function (err, result)
            {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({emptyLand: result}));
                response.end();
            });
        });

    }
}

/** end newly added function **/
function checkMarchResult(request, response, next) {
    console.log("called match result");
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            dbMarches.getMarchResult(post.marchId, function (row) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                console.log(row);
                if (row != undefined) {

                    response.write(JSON.stringify({matchResult: row.status_id}));

                } else {
                    response.write(JSON.stringify({matchResult: 40, error: "no match found"}));
                }

                response.end();

            });

        });
    }

}

function attack(request, response, next) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            console.log('attack was triggered');
            //todo use post method for the id's
            //battleStateInstance.startWar();
//            var unitsSend = JSON.parse(post.units);JSON.parse('{ "property":"value" }');
            var unitsSend = {};
            if (post.units != undefined) {
                unitsSend = JSON.parse(post.units);
            }
            var min_speed = 100;
            __.each(unitsSend, function (element, index, list) {
                min_speed = dbMatrix.battleUnits[element.unitType].speed < min_speed ? dbMatrix.battleUnits[element.unitType].speed : min_speed;
            });
            console.log('distance:', post.distance);
            console.log('min_speed:', min_speed);
            console.log('time_arrival:', computeTimeArrival(post.distance, min_speed));
            console.log('units:', unitsSend);
            var attackCityId = post.attackCityId;
            var defendCityId = post.defendCityId;

            var march_type = post.attackType, target_city_type = post.targetType,
                    target_city_user_id = post.targetCityUserId, current_city_user_id = post.userId,
                    knightno = 1, current_city = attackCityId, target_city = defendCityId,
                    object_type = 1, object_id = 1, speed = min_speed, distance = post.distance, upkeep = 1,
                    oneway_trip = 1, status = 1, arraival_in_seconds = 10, target_city_is_owned = post.isOwned;
            //arraival_in_seconds = computeTimeArrival(post.distance, min_speed);
            arraival_in_seconds = 10;
            console.log('attackCity Id:', defendCityId);

            dbMarches.insertMarches(
                    march_type, knightno, current_city, target_city,
                    object_type, object_id, speed, distance, upkeep,
                    oneway_trip, status, arraival_in_seconds, unitsSend, target_city_type, current_city_user_id, target_city_user_id, target_city_is_owned, doAttack, function (lastInsertId) {
                        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                        response.write(JSON.stringify({arrivalTime: arraival_in_seconds, marchId: lastInsertId}));
                        response.end();

                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.userId = target_city_user_id;
                        reportItem.cityId = target_city;
                        reportItem.title = "An Enemy will Attack your city";//TODO Language Pack
                        reportItem.reportType = 10;
                        reportItem.marchId = lastInsertId;
                        rs.ReportSystem.addUserReport(target_city_user_id, reportItem, function () {});
                    }
            );
        });
    }

}

function doAttack(marchId) {
    var marchObj;
    var attackTroopsUnits;
    dbMarches.getMarchById(marchId, function (rowFetched) {
        marchObj = rowFetched;
        //get all deployed units
        dbMarches.getMarchUnitByMarchId(marchId, function (err, result) {
            attackTroopsUnits = result;
            //check for the attack type before attacking
            switch (marchObj.march_type) {
                case 10:
                    //attack
                    marchAttack(marchObj, attackTroopsUnits);
                    break;
                case 20:
                    //colonize
                    marchColonize(marchObj, attackTroopsUnits);
                    break;
                case 30:
                    //convert
                    marchConvert(marchObj, attackTroopsUnits);
                    break;
                case 40:
                    //scout
                    marchScout(marchObj, attackTroopsUnits);
                    break;
                case 50:
                    //destroy
                    marchDestroy(marchObj, attackTroopsUnits);
                    break;
            }



        });
    });
}

function marchAttack(marchObj, attackTroopsUnits) {

    //check city type first before getting the defending troops
    switch (marchObj.target_city_type) {
        case 0:
            //attacking empty land
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    dtu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie

                    if (warResult == 10) {
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Won the Attack on an Empty. It's now yours.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //put alive troops on empty land & change owner
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Sending troops to the Empty land. ---------------------------");
                            dbPlayerInformation.deleteAllUnitsFromCity(marchObj.target_city, function () {

                            });
                            var aliveTroops = attackTroops.troopsAlive;
                            __.each(aliveTroops, function (element, index, list) {
                                cityUnit.addMultipleUnitToCity(marchObj.target_city, element.unit, element.unitCount);
                            });
                            //changing the owner
                            dbPlayerInformation.changeOwnerOfCity(marchObj.target_city, marchObj.current_city_user_id, function () {

                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            if (marchObj.target_city_is_owned == "1") {
                                reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                            }
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else if (warResult == 20) {

                        //Defender won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Lose the Attack on an Empty Land. It will stay with the Current Owner";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            if (marchObj.target_city_is_owned == "1") {
                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                var reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "Another User Attacked your Empty Land.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                            }

                            //decrease defenders's city stats
//                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                            //decrease defenders's city stats
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else {

                        //tied
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "Your Attack on an Empty Land Ended in a Draw.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            setTimeout(function () {
                                //add the troops to the city & add resource to the city
                                //        dbPlayerInformation
                                console.log("Sending back the Alive Troops. -----------------------");
                                __.each(aliveTroops, function (element, index, list) {
                                    cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                    var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                });
                            }, 10 * 1000);

                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //do the report if human
                            if (marchObj.target_city_is_owned == "1") {
                                reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "You Successfuly defended an Empty Land.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                            }
                        }
                        /*----------------------------------------------------------------------------------------*/
                    }
                });
            });
            break;
        case 1:
            //attacking human city
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    dtu.addTroops(unitObj, element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(unitObj, element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie

                    if (warResult == 10) {
                        //attacker won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Won the Attack.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city & getting resources
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                        reportItem.userId = marchObj.target_city_user_id;
                        reportItem.cityId = marchObj.target_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
                            var cityStatChange = {courage: -3, happiness: -3, points: attackerPoints, cityId: marchObj.target_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });
                        }
                        /*----------------------------------------------------------------------------------------*/


                    } else if (warResult == 20) {
                        //Defender won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Lose the Attack.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Succeeded to Defend your City from an Attack.";//TODO Language Pack
                        reportItem.userId = marchObj.target_city_user_id;
                        reportItem.cityId = marchObj.target_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });
                        }
                        /*----------------------------------------------------------------------------------------*/

                    } else {
                        //tied
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "Your Attack Ended in a Draw.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city & getting resources
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            capacity = capacity / 2;
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "Your Defend Ended up in a Draw.";//TODO Language Pack
                        reportItem.userId = marchObj.target_city_user_id;
                        reportItem.cityId = marchObj.target_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
                            var cityStatChange = {courage: -1, happiness: -1, points: attackerPoints, cityId: marchObj.target_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });
                        }
                        /*----------------------------------------------------------------------------------------*/
                    }
                });
            });
            break;
        case 10:
            //attacking barbarian city
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    dtu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie

                    if (warResult == 10) {
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Won the Attack on a Barbarian City.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city & getting resources
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    //end delete this if its a barbarian city
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
//                            var cityStatChange = {courage: -3, happiness: -3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                            //END decrease defenders's city stats
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else if (warResult == 20) {

                        //Defender won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Lose the Attack on a Barbarian City.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
//                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                            //decrease defenders's city stats
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else {

                        //tied
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "Your Attack on a Barbarian City Ended in a Draw.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city & getting resources
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            capacity = capacity / 2;
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
//                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //remove killed units from Defending City
                            var fallenTroops = [];
                            __.each(defendTroops.troopsCollection, function (element, index, list) {
                                if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                    var troopsAliveUnit = defendTroops.troopsAlive[index];
                                    var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                } else {
                                    var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                    fallenTroops.push(killedUnit);
                                    //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                }
                            });
                            cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                            });
                            //decrease defenders's city stats
//                            var cityStatChange = {courage: -1, happiness: -1, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                        }
                        /*----------------------------------------------------------------------------------------*/
                    }
                });
            });
            break;
        case 20:
            //attacking city fields
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    dtu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie

                    if (warResult == 10) {
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Won the Attack on a Valley. It's now yours.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //Get all the resource you can get
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    //end delete this if its a barbarian city
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            if (marchObj.target_city_is_owned == "1") {
                                reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "You Failed to Defend your Valley from an Attack.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                                //decrease defenders's city stats
                                var cityStatChange = {courage: -1, happiness: -1, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.target_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else if (warResult == 20) {

                        //Defender won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Lose the Attack on an Valley. It will stay with the Current Owner";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            if (marchObj.target_city_is_owned == "1") {
                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                var reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "Another User Attacked your Empty Land.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                            }

                            //decrease defenders's city stats
//                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                            //decrease defenders's city stats
                        }
                        /*----------------------------------------------------------------------------------------*/
                    } else {

                        //tied
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "Your Attack on a Valley Ended in a Draw.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 10;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                        //get alive troops for returning to city
                        /*----------------------------------------------------------------------------------------*/

                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    //end delete this if its a barbarian city
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }


                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });


                            //do the report if human
                            if (marchObj.target_city_is_owned == "1") {

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });

                                reportItem = ReportItem();
                                reportItem.serverInsert = 1;
                                reportItem.serverInsertId = Date.now();
                                reportItem.title = "You Successfuly defended a Valley.";//TODO Language Pack
                                reportItem.userId = marchObj.target_city_user_id;
                                reportItem.cityId = marchObj.target_city;
                                reportItem.reportType = 10;
                                reportItem.marchId = marchObj.id;

                                rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                            }
                        }
                        /*----------------------------------------------------------------------------------------*/
                    }
                });
            });
            break;
    }
}



function calculateResourceBasedOnTroopCapacity(aliveTroops) {
    console.log("Calculating Resource Based on Troop capacity ------------------------------------");
    var capacity = 0;
    __.each(aliveTroops, function (element, index, list) {
        var unitCapacity = parseInt(element.unit.capacity);
        var unitCount = parseInt(element.unitCount);
        var unitTotalCapacity = unitCapacity * unitCount;
        capacity += unitTotalCapacity;

    });
    console.log("Capacity is :" + capacity);
    return capacity;
}

function marchColonize(marchObj, attackTroopsUnits) {

    //check city type first before getting the defending troops
    switch (marchObj.target_city_type) {
        case 1:
            //attacking human city
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    dtu.addTroops(unitObj, element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(unitObj, element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {

                    dbPlayerInformation.getCityHappinessAndCourage(marchObj.target_city, function (err, result) {
                        //warResult : 10 = attacker won, 20 = defender won, 30 = tie
                        if (warResult == 10 && result[0].happiness == 0 && result[0].courage == 0) {
                            //attacker won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Won the Attack. Enemy City Was Destroyed";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});
                            //destroy the city
                            dcs.DestroyCitySystem.addDestroyCityToUser(marchObj.target_city_user_id, {cityId: marchObj.target_city});
                            dbPlayerInformation.downgradeToLevel1AllBuildings(marchObj.target_city, function () {

                            });
                            //get alive troops for returning to city & getting resources
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: -3, happiness: -3, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/


                        } else if (warResult == 20) {
                            //Defender won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Lose the Destroy Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Succeeded to Defend your City from a Destroy Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/

                        } else {
                            //tied
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Attack Ended in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //get alive troops for returning to city & getting resources
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                capacity = capacity / 2;
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Defend Ended up in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: -1, happiness: -1, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/
                        }
                    });

                });
            });
            break;

        case 20:
            //Destroying city fields
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    dtu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie
                    dbPlayerInformation.getCityHappinessAndCourage(marchObj.target_city, function (err, result) {
                        if (warResult == 10 && result[0].happiness == 0 && result[0].courage == 0) {
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Won Destroying a Valley.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //Get all the resource you can get
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                        //end delete this if its a barbarian city
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                if (marchObj.target_city_is_owned == "1") {
                                    reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                    //TODO valley put the level to 1
                                }
                            }
                            /*----------------------------------------------------------------------------------------*/
                        } else if (warResult == 20) {

                            //Defender won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Lose the Attack on a Valley. It will stay with the Current Owner";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                if (marchObj.target_city_is_owned == "1") {
                                    //remove killed units from Defending City
                                    var fallenTroops = [];
                                    __.each(defendTroops.troopsCollection, function (element, index, list) {
                                        if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                            var troopsAliveUnit = defendTroops.troopsAlive[index];
                                            var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        } else {
                                            var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        }
                                    });
                                    cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                    });
                                    var reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "Another User Attacked your Valley.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                }

                                //decrease defenders's city stats
//                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                                //decrease defenders's city stats
                            }
                            /*----------------------------------------------------------------------------------------*/
                        } else {

                            //tied
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Attack on a Valley in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //get alive troops for returning to city
                            /*----------------------------------------------------------------------------------------*/

                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                        //end delete this if its a barbarian city
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }


                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });


                                //do the report if human
                                if (marchObj.target_city_is_owned == "1") {

                                    //remove killed units from Defending City
                                    var fallenTroops = [];
                                    __.each(defendTroops.troopsCollection, function (element, index, list) {
                                        if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                            var troopsAliveUnit = defendTroops.troopsAlive[index];
                                            var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        } else {
                                            var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        }
                                    });
                                    cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                    });

                                    reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "You Successfuly defended a Valley.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                }
                            }
                            /*----------------------------------------------------------------------------------------*/
                        }
                    });

                });
            });
            break;
    }
}

function marchConvert(marchObj, attackTroopsUnits) {
    switch (marchObj.target_city_type) {
        case 0:
            //attacking empty land
            break;
        case 1:
            //attacking human city

            break;
        case 10:
            //attacking barbarian city
            break;
        case 20:
            //attacking city fields
            break;
    }
}

function marchScout(marchObj, attackTroopsUnits) {
    switch (marchObj.target_city_type) {
        case 0:
            //Spy On empty land
            break;
        case 1:
            //Spy On human city
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                var defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    dtu.addTroops(unitObj, element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(unitObj, element.unitcount);
                });

                //make them fight!
                console.log('start Scout');
                battleStateInstance.startScout(dtu, atu, marchObj.id, function (revealPercentage, attackTroops, defenceTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie

                    if (revealPercentage == 100) {
                        //attacker won
                        /*------------------------ attacker part ----------------------------- */
                        console.log('Adding report Item to the Report System');
                        var reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Successfully spied on a City.";//TODO Language Pack
                        reportItem.userId = marchObj.current_city_user_id;
                        reportItem.cityId = marchObj.current_city;
                        reportItem.reportType = 20;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function (err, result) {
                            dbReportScoutUnit.insertReportScoutUnitMultiple(result.insertId, marchObj.target_city, defenceTroops.troopsAlive, function () {});
                        });

                        //get alive troops for returning to city & getting resources
                        /*----------------------------------------------------------------------------------------*/
                        {
                            console.log("Alive Troops marching back to city. ---------------------------");
                            //calculate the capacity of alive troops
                            var aliveTroops = attackTroops.troopsAlive;
                            var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                            //reduce the resources of the target city
                            dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                var food_value;
                                var wood_value;
                                var stone_value;
                                var iron_value;
                                var copper_value;
                                var silver_value;
                                var gold_value;
                                food_value = parseInt(result[0].food);
                                food_value = capacity < food_value ? capacity : food_value;
                                wood_value = parseInt(result[0].wood);
                                wood_value = capacity < wood_value ? capacity : wood_value;
                                stone_value = parseInt(result[0].stone);
                                stone_value = capacity < stone_value ? capacity : stone_value;
                                iron_value = parseInt(result[0].iron);
                                iron_value = capacity < iron_value ? capacity : iron_value;
                                copper_value = parseInt(result[0].copper);
                                copper_value = capacity < copper_value ? capacity : copper_value;
                                silver_value = parseInt(result[0].silver);
                                silver_value = capacity < silver_value ? capacity : silver_value;
                                gold_value = parseInt(result[0].gold);
                                gold_value = capacity < gold_value ? capacity : gold_value;
                                var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                    console.log("Deducting Resources:");
                                    console.log(resourceCost);
                                    //delete this if its a barbarian city
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                });

                                //add resources to the attacking city after the marching
                                var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                setTimeout(function () {
                                    //add the troops to the city & add resource to the city
                                    //        dbPlayerInformation
                                    console.log("Sending back the Alive Troops. -----------------------");
                                    __.each(aliveTroops, function (element, index, list) {
                                        cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                        var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                    });
                                    console.log("Transferring the Resource to the attacker's city");
                                    //deliver items to the Attacker's City
                                    var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                    console.log("Transfered Resource:");
                                    console.log(resourceCost);
                                    dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                    });
                                    rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                }, 10 * 1000);
                            });
                        }

                        /*----------------------------------------------------------------------------------------*/
                        //marchBackToCity(marchObj, attackTroops.troopsAlive);

                        /*------------------------------------ Defender part --------------------- */
                        reportItem = ReportItem();
                        reportItem.serverInsert = 1;
                        reportItem.serverInsertId = Date.now();
                        reportItem.title = "You Failed to Defend your City from a Spy Attack.";//TODO Language Pack
                        reportItem.userId = marchObj.target_city_user_id;
                        reportItem.cityId = marchObj.target_city;
                        reportItem.reportType = 20;
                        reportItem.marchId = marchObj.id;

                        rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                        //marchbacktocity equivalent for defending city
                        /*----------------------------------------------------------------------------------------*/
                        {
                            //increase attacker's city stats
                            var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                            });

                            //decrease defenders's city stats
                            var cityStatChange = {courage: -3, happiness: -3, points: attackerPoints, cityId: marchObj.target_city};
                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                            dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                            });
                        }
                        /*----------------------------------------------------------------------------------------*/


                    }
                });
            });
            break;
        case 10:
            //attacking barbarian city
            break;
        case 20:
            //attacking city fields
            break;
    }
}

function marchDestroy(marchObj, attackTroopsUnits) {
    //check city type first before getting the defending troops
    switch (marchObj.target_city_type) {
        case 1:
            //Destroy human city
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    dtu.addTroops(unitObj, element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(unitObj, element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {

                    dbPlayerInformation.getCityHappinessAndCourage(marchObj.target_city, function (err, result) {
                        //warResult : 10 = attacker won, 20 = defender won, 30 = tie
                        if (warResult == 10 && result[0].happiness == 0 && result[0].courage == 0) {
                            //attacker won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Won the Attack. Enemy City Was Destroyed";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});
                            //destroy the city
                            dcs.DestroyCitySystem.addDestroyCityToUser(marchObj.target_city_user_id, {cityId: marchObj.target_city});
                            dbPlayerInformation.downgradeToLevel1AllBuildings(marchObj.target_city, function () {

                            });
                            //get alive troops for returning to city & getting resources
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: -3, happiness: -3, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/


                        } else if (warResult == 20) {
                            //Defender won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Lose the Destroy Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Succeeded to Defend your City from a Destroy Attack.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/

                        } else {
                            //tied
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Attack Ended in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //get alive troops for returning to city & getting resources
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                capacity = capacity / 2;
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Defend Ended up in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.target_city_user_id;
                            reportItem.cityId = marchObj.target_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                //remove killed units from Defending City
                                var fallenTroops = [];
                                __.each(defendTroops.troopsCollection, function (element, index, list) {
                                    if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                        var troopsAliveUnit = defendTroops.troopsAlive[index];
                                        var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    } else {
                                        var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                        fallenTroops.push(killedUnit);
                                        ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                    }
                                });
                                cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                });
                                //decrease defenders's city stats
                                var cityStatChange = {courage: -1, happiness: -1, points: attackerPoints, cityId: marchObj.target_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });
                            }
                            /*----------------------------------------------------------------------------------------*/
                        }
                    });

                });
            });
            break;

        case 20:
            //Destroying city fields
            dbMarches.getUnitsFromCity(marchObj.target_city, function (err, result) {
                console.log(result);
                console.log("number of defending troops:", result.length);
                console.log("number of attacking troops:", attackTroopsUnits.length);
                defenceTroopsUnits = result;
                var dtu = troops(); //defence troops unit obj list
                var atu = troops(); //attack troops unit obj list
                var attackerPoints = 0;
                __.each(defenceTroopsUnits, function (element, index, list) {
                    dtu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitscount);
                });
                __.each(attackTroopsUnits, function (element, index, list) {
                    var unitObj = dbMatrix.battleUnits[element.unit_type];
                    attackerPoints += parseInt(unitObj.population) * parseInt(element.unitcount);
                    atu.addTroops(dbMatrix.battleUnits[element.unit_type], element.unitcount);
                });
                console.log('defence_troop:', defenceTroopsUnits);
                //make them fight!
                console.log('start attack');
                battleStateInstance.startWar(dtu, atu, marchObj.id, function (warResult, attackTroops, defendTroops) {
                    //warResult : 10 = attacker won, 20 = defender won, 30 = tie
                    dbPlayerInformation.getCityHappinessAndCourage(marchObj.target_city, function (err, result) {
                        if (warResult == 10 && result[0].happiness == 0 && result[0].courage == 0) {
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Won Destroying a Valley.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //Get all the resource you can get
                            /*----------------------------------------------------------------------------------------*/
                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                        //end delete this if its a barbarian city
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 2, happiness: 2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                if (marchObj.target_city_is_owned == "1") {
                                    reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "You Failed to Defend your City from an Attack.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                    //TODO valley put the level to 1
                                }
                            }
                            /*----------------------------------------------------------------------------------------*/
                        } else if (warResult == 20) {

                            //Defender won
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "You Lose the Attack on a Valley. It will stay with the Current Owner";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */

                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: -2, happiness: -2, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.reduceCityStat(marchObj.current_city, cityStatChange, function () {

                                });

                                if (marchObj.target_city_is_owned == "1") {
                                    //remove killed units from Defending City
                                    var fallenTroops = [];
                                    __.each(defendTroops.troopsCollection, function (element, index, list) {
                                        if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                            var troopsAliveUnit = defendTroops.troopsAlive[index];
                                            var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        } else {
                                            var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        }
                                    });
                                    cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                    });
                                    var reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "Another User Attacked your Valley.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                }

                                //decrease defenders's city stats
//                            var cityStatChange = {courage: 3, happiness: 3, points: attackerPoints, cityId: marchObj.target_city};
//                            rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
//                            dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {
//
//                            });
                                //decrease defenders's city stats
                            }
                            /*----------------------------------------------------------------------------------------*/
                        } else {

                            //tied
                            /*------------------------ attacker part ----------------------------- */
                            console.log('Adding report Item to the Report System');
                            var reportItem = ReportItem();
                            reportItem.serverInsert = 1;
                            reportItem.serverInsertId = Date.now();
                            reportItem.title = "Your Attack on a Valley in a Draw.";//TODO Language Pack
                            reportItem.userId = marchObj.current_city_user_id;
                            reportItem.cityId = marchObj.current_city;
                            reportItem.reportType = 10;
                            reportItem.marchId = marchObj.id;

                            rs.ReportSystem.addUserReport(marchObj.current_city_user_id, reportItem, function () {});

                            //get alive troops for returning to city
                            /*----------------------------------------------------------------------------------------*/

                            {
                                console.log("Alive Troops marching back to city. ---------------------------");
                                //calculate the capacity of alive troops
                                var aliveTroops = attackTroops.troopsAlive;
                                var capacity = calculateResourceBasedOnTroopCapacity(aliveTroops);
                                //reduce the resources of the target city
                                dbPlayerInformation.getResourcesFromCity(marchObj.target_city, function (err, result) {
                                    var food_value;
                                    var wood_value;
                                    var stone_value;
                                    var iron_value;
                                    var copper_value;
                                    var silver_value;
                                    var gold_value;
                                    food_value = parseInt(result[0].food);
                                    food_value = capacity < food_value ? capacity : food_value;
                                    wood_value = parseInt(result[0].wood);
                                    wood_value = capacity < wood_value ? capacity : wood_value;
                                    stone_value = parseInt(result[0].stone);
                                    stone_value = capacity < stone_value ? capacity : stone_value;
                                    iron_value = parseInt(result[0].iron);
                                    iron_value = capacity < iron_value ? capacity : iron_value;
                                    copper_value = parseInt(result[0].copper);
                                    copper_value = capacity < copper_value ? capacity : copper_value;
                                    silver_value = parseInt(result[0].silver);
                                    silver_value = capacity < silver_value ? capacity : silver_value;
                                    gold_value = parseInt(result[0].gold);
                                    gold_value = capacity < gold_value ? capacity : gold_value;
                                    var resourceCost = {food: -food_value, wood: -wood_value, stone: -stone_value, iron: -iron_value, copper: -copper_value, silver: -silver_value, gold: -gold_value};
                                    dbPlayerInformation.reduceResourcesFromCity(marchObj.target_city, resourceCost, function (err, result) {
                                        console.log("Deducting Resources:");
                                        console.log(resourceCost);
                                        //delete this if its a barbarian city
                                        //rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.target_city, resourceCost);
                                        //end delete this if its a barbarian city
                                    });

                                    //add resources to the attacking city after the marching
                                    var arraival_in_sec = computeTimeArrival(marchObj.distance, marchObj.speed);
                                    setTimeout(function () {
                                        //add the troops to the city & add resource to the city
                                        //        dbPlayerInformation
                                        console.log("Sending back the Alive Troops. -----------------------");
                                        __.each(aliveTroops, function (element, index, list) {
                                            cityUnit.addMultipleUnitToCity(marchObj.current_city, element.unit, element.unitCount);
                                            var killedUnit = {unitName: element.unit.name, unitCount: element.unitCount, cityId: marchObj.current_city};
                                            ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.current_city, killedUnit);
                                        });
                                        console.log("Transferring the Resource to the attacker's city");
                                        //deliver items to the Attacker's City
                                        var resourceCost = {food: food_value, wood: wood_value, stone: stone_value, iron: iron_value, copper: copper_value, silver: silver_value, gold: gold_value, cityId: marchObj.current_city};
                                        console.log("Transfered Resource:");
                                        console.log(resourceCost);
                                        dbPlayerInformation.increaseResourcesFromCity(marchObj.current_city, resourceCost, function () {

                                        });
                                        rr.ReduceResourceSystem.addReduceResourceToUser(marchObj.current_city, resourceCost);
                                    }, 10 * 1000);
                                });
                            }


                            /*----------------------------------------------------------------------------------------*/
                            //marchBackToCity(marchObj, attackTroops.troopsAlive);

                            /*------------------------------------ Defender part --------------------- */
                            //marchbacktocity equivalent for defending city
                            /*----------------------------------------------------------------------------------------*/
                            {
                                //increase attacker's city stats
                                var cityStatChange = {courage: 1, happiness: 1, points: attackerPoints, cityId: marchObj.current_city};
                                rcs.ReduceCityStatSystem.addReduceCityStatToUser(marchObj.current_city_user_id, cityStatChange);
                                dbCityStats.increaseCityStat(marchObj.current_city, cityStatChange, function () {

                                });


                                //do the report if human
                                if (marchObj.target_city_is_owned == "1") {

                                    //remove killed units from Defending City
                                    var fallenTroops = [];
                                    __.each(defendTroops.troopsCollection, function (element, index, list) {
                                        if (defendTroops.troopsAlive.hasOwnProperty(index)) {
                                            var troopsAliveUnit = defendTroops.troopsAlive[index];
                                            var killedUnit = {unitName: element.unit.name, unitCount: -(element.unitCount - troopsAliveUnit.unitCount), cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        } else {
                                            var killedUnit = {unitName: element.unit.name, unitCount: -element.unitCount, cityId: marchObj.target_city};
                                            fallenTroops.push(killedUnit);
                                            //ru.ReduceUnitSystem.addReduceUnitToUser(marchObj.target_city, killedUnit);
                                        }
                                    });
                                    cityUnit.killManyTroops(fallenTroops, marchObj.target_city, function () {

                                    });

                                    reportItem = ReportItem();
                                    reportItem.serverInsert = 1;
                                    reportItem.serverInsertId = Date.now();
                                    reportItem.title = "You Successfuly defended a Valley.";//TODO Language Pack
                                    reportItem.userId = marchObj.target_city_user_id;
                                    reportItem.cityId = marchObj.target_city;
                                    reportItem.reportType = 10;
                                    reportItem.marchId = marchObj.id;

                                    rs.ReportSystem.addUserReport(marchObj.target_city_user_id, reportItem, function () {});
                                }
                            }
                            /*----------------------------------------------------------------------------------------*/
                        }
                    });

                });
            });
            break;
    }
}

function doRegistration(request, response) {

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            dbPlayerInformation.registrateUser(post.username, post.username, post.password, post.email, post.emperor, post.languag, '1', 1000, 0, function (err, result) {});
        });

    }
}

function doLogin(request, response) {

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            dbPlayerInformation.getUser(post.username, post.password, function (err, result) {
                var now = new Date();
                var serverTimeString =  now.getMonth() + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
                if (result.length > 0) {
                    //do login
                    console.log("Found User:", result);
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: "0", userObj: result[0], serverTime: serverTimeString}));
                    response.end();
                } else {
                    console.log("No user Found.");
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: "10", serverTime: serverTimeString}));
                    response.end();
                }
            });
        });

    }

}


function trainTroops(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var units = JSON.parse(post.units);
            var unitsCost = JSON.parse(post.unitCosts);
            console.log(units);
            //reduce city resources
            cityResource.reduceCityResourceMany(post.cityId, unitsCost, function () {
                __.each(units, function (element, index, list) {
                    cityUnit.trainTroops(dbMatrix.unitMatrix[element.unitType], dbMatrix.unitMatrix[element.unitType], element.unitCount, post.cityId);
                    //dbMatrix.battleUnits[element.unitType], element.unitcount;
                });
            });
        });

    }
}

function getAllUnitsByUser(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllUnitsByUser(post.userId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({units: result}));
                response.end();
            });
        });

    }
}

function getAllUnitsFromCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllUnitsFromCity(post.city_id, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({units: result}));
                response.end();
            });
        });

    }
}

function getAllFieldsByUser(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllFieldsByUser(post.userId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cityFields: result}));
                response.end();
            });
        });
    }

}

function getAllFieldsFromCity() {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllBuildingsFromCity(post.city_id, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cityFields: result}));
                response.end();
            });
        });
    }

}

function getAllBuildingsByUser(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllBuildingsByUser(post.userId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cityBuilding: result}));
                response.end();
            });
        });
    }

}

function getAllBuildingsFromCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllBuildingsFromCity(post.city_id, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cityBuilding: result}));
                response.end();
            });
        });
    }
}

function getAllFieldsFromCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getAllFieldsFromCity(post.city_id, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cityFields: result}));
                response.end();
            });
        });
    }

}

function getUserCities(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getUserCities(post.userId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({cities: result}));
                response.end();
            });
        });
    }
}

function getResourcesFromCity(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);

            dbPlayerInformation.getResourcesFromCity(post.cityId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({resource: result}));
                response.end();
            });
        });
    }

}

/******************* INITIALIZATION OF MATRIX ***********************/
function getAllMatrix(request, response) {
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({
        unitMatrix: dbMatrix.unitMatrix,
        languagePackEn: dbMatrix.languagePackEn.result,
        fieldsMatrix: dbMatrix.fieldsMatrix.result,
        fieldLevelMatrix: dbMatrix.fieldsLevelMatrix.result,
        fieldMatrixUpCost: dbMatrix.fieldsMatrixUpCosts.result,
        fieldMatrixDownCost: dbMatrix.fieldsMatrixDownCosts.result,
        buildingMatrix: dbMatrix.buildingMatrix.result,
        buildingMatrixUpCost: dbMatrix.buildingMatrixUpCost.result,
        buildingMatrixDownCost: dbMatrix.buildingMatrixDownCost.result
    }));
    response.end();
}
/******************* END INITIALIZATION OF MATRIX ***********************/

function getAllUnitMatrix(request, response) {
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({unitMatrix: dbMatrix.unitMatrix}));
    response.end();
}

function getAllFieldsMatrix(request, response) {
//    dbMatrix.getAllFieldsMatrix(function (err, result) {
//        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//        response.write(JSON.stringify({fieldsMatrix: result}));
//        response.end();
//    });

    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fieldsMatrix: dbMatrix.fieldsMatrix.result}));
    response.end();
}

function getAllFieldLevelMatrix(request, response) {
//    dbMatrix.getAllFieldLevelMatrix(function (err, result) {
//        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//        response.write(JSON.stringify({fieldLevelMatrix: result}));
//        response.end();
//    });
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fieldLevelMatrix: dbMatrix.fieldsLevelMatrix.result}));
    response.end();
}

function getAllFieldMatrixUpCost(request, response) {
//    dbMatrix.getAllFieldMatrixUpCost(function (err, result) {
//        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//        response.write(JSON.stringify({fieldMatrixUpCost: result}));
//        response.end();
//    });
    console.log("getting filedsMatrixupcost count:" + dbMatrix.fieldsMatrixUpCosts.length);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fieldMatrixUpCost: dbMatrix.fieldsMatrixUpCosts.result}));
    response.end();
}

function getAllBuildingMatrix(request, response) {
//    dbMatrix.getAllBuldingMatrix(function (err, result) {
//        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//        response.write(JSON.stringify({buildingMatrix: result}));
//        response.end();
//    });

    console.log("Getting BuildingMatrix Count: " + dbMatrix.buildingMatrix.result.length);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({buildingMatrix: dbMatrix.buildingMatrix.result}));
    response.end();
}

function getAllBuildingMatrixUpCost(request, response) {
//    dbMatrix.getAllBuildingMatrixUpCost(function (err, result) {
//        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//        response.write(JSON.stringify({buildingMatrixUpCost: result}));
//        response.end();
//    });
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({buildingMatrixUpCost: dbMatrix.buildingMatrixUpCost.result}));
    response.end();

}

/**
 * 
 * @param {type} request //id of the city_building & timeCompletion
 * @param {type} response
 * @returns {undefined}
 */
function destroyDowngradeCityField(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var fieldBuilding = JSON.parse(post.fieldBuilding);
            var fieldLevelMatrix = JSON.parse(post.fieldLevelMatrix);
            var resourceCost = JSON.parse(post.resourceCost);
            console.log("Printing field level matrix ----------------------------------------------");
            console.log(fieldLevelMatrix);
            if (fieldLevelMatrix.level == 0) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({status: 0}));
                response.end();
                dbPlayerInformation.setCityFieldStatus(fieldBuilding.id, 10, function () {}); //set the status of the city field
                //status set to 0 for idle
                dbPlayerInformation.reduceResourcesFromCity(fieldBuilding.cityId, resourceCost, function (err, result) {
                    console.log("Deducting Resources");
                });
                setTimeout(function () {
                    dbPlayerInformation.destroyCityField(fieldBuilding, function (err, result) {
                        console.log("Destroy Complete. City Building:" + post.id);
                    });
                }, post.timeCompletion * 1000);
            } else {
                dbPlayerInformation.setCityFieldStatus(fieldBuilding, 10, post.timeCompletion, fieldLevelMatrix, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0, insertId: -1}));
                    response.end();
                    //status set to 0 for idle
                    dbPlayerInformation.reduceResourcesFromCity(fieldBuilding.cityId, resourceCost, function (err, result) {
                        console.log("Deducting Resources:" + resourceCost);
                    });
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.downgradeCityField(post.id, 0, fieldLevelMatrix, function (err, result) {
                            console.log("Downgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            }

        });
    }
}


/**
 * 
 * @param {type} request //id of the city_building & timeCompletion
 * @param {type} response
 * @returns {undefined}
 */
function createUpgradeCityField(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var fieldBuilding = JSON.parse(post.fieldBuilding);
            var fieldLevelMatrix = JSON.parse(post.fieldLevelMatrix);
            var resourceCost = JSON.parse(post.resourceCost);
            console.log(post.fieldBuilding);
            if (fieldBuilding.id == 0) {
                dbPlayerInformation.createCityField(fieldBuilding, 10, post.timeCompletion, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0, insertId: result.insertId}));
                    response.end();
                    //status set to 0 for idle
                    dbPlayerInformation.reduceResourcesFromCity(fieldBuilding.cityId, resourceCost, function (err, result) {
                        console.log("Deducting Resources");
                    });
                    setTimeout(function () {

                        dbPlayerInformation.upgradeCityField(result.insertId, 0, fieldLevelMatrix, function (err, result) {
                            console.log("Upgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            } else {
                dbPlayerInformation.setCityFieldStatus(fieldBuilding, 10, post.timeCompletion, fieldLevelMatrix, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0, insertId: -1}));
                    response.end();
                    //status set to 0 for idle
                    dbPlayerInformation.reduceResourcesFromCity(fieldBuilding.cityId, resourceCost, function (err, result) {
                        console.log("Deducting Resources:" + resourceCost);
                    });
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.upgradeCityField(post.id, 0, fieldLevelMatrix, function (err, result) {
                            console.log("Upgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            }

        });
    }
}

/**
 * 
 * @param {type} request //id of the city_building & timeCompletion
 * @param {type} response
 * @returns {undefined}
 */
function destroyDowngradeCityBuilding(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var cityBuilding = JSON.parse(post.cityBuilding);
            var resourceCost = JSON.parse(post.resourceCost);
            console.log(cityBuilding);
            console.log(cityBuilding.buildingMatrix.food);
            if (cityBuilding.level == 1) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({status: 0}));
                response.end();
                dbPlayerInformation.reduceResourcesFromCity(post.id, resourceCost, function (err, result) {
                    console.log("Deducting Resources:" + resourceCost);
                });
                dbPlayerInformation.setCityBuildingStatus(cityBuilding, 10, post.timeCompletion, function (err, result) {
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.destroyCityBuilding(post.id, function (err, result) {
                            console.log("Destroy Building Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });

            } else {
                dbPlayerInformation.setCityBuildingStatus(cityBuilding, 10, post.timeCompletion, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0}));
                    response.end();
                    dbPlayerInformation.reduceResourcesFromCity(post.id, resourceCost, function (err, result) {
                        console.log("Deducting Resources:" + resourceCost);
                    });
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.downgradeCityBuilding(post.id, 0, function (err, result) {
                            console.log("Downgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            }

        });
    }
}

/**
 * 
 * @param {type} request //id of the city_building & timeCompletion
 * @param {type} response
 * @returns {undefined}
 */
function createUpgradeCityBuilding(request, response) {
    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            var cityBuilding = JSON.parse(post.cityBuilding);
            var resourceCost = JSON.parse(post.resourceCost);
            console.log(cityBuilding);
            console.log(cityBuilding.buildingMatrix.food);
            if (cityBuilding.id == 0) {
                dbPlayerInformation.createCityBuilding(cityBuilding, 10, post.timeCompletion, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0, insertId: result.insertId}));
                    response.end();
                    dbPlayerInformation.reduceResourcesFromCity(cityBuilding.cityId, resourceCost, function (err, result) {
                        console.log("Deducting Resources:" + resourceCost);
                    });
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.upgradeCityBuilding(result.insertId, 0, function (err, result) {
                            console.log("Upgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            } else {
                dbPlayerInformation.setCityBuildingStatus(cityBuilding, 10, post.timeCompletion, function (err, result) {
                    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    response.write(JSON.stringify({status: 0, insertId: -1}));
                    response.end();
                    dbPlayerInformation.reduceResourcesFromCity(cityBuilding.cityId, resourceCost, function (err, result) {
                        console.log("Deducting Resources:" + resourceCost);
                    });
                    setTimeout(function () {
                        //status set to 0 for idle
                        dbPlayerInformation.upgradeCityBuilding(post.id, 0, function (err, result) {
                            console.log("Upgrade Complete. City Building:" + post.id);
                        });
                    }, post.timeCompletion * 1000);
                });
            }

        });
    }
}

function getAllTest(request, response) {

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
//            post.username
            raymondTestTable.getAllTestData1(post.allianceNo, function (err, result, testString) {
                console.log(testString);
                console.log(result);
                for (var counter = 0; counter < result.length; counter++) {
                    console.log(result[counter].id);
                    console.log(result[counter].name);
                }
            });
        });
    }
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({serverReply: "all ok"}));
    response.end();

}

function rainAllianceChat(request, response) {

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);



            rainLocalChat.getAllTestData(1, function (err, result) {
                console.log(result);


                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedObject: result}));
                response.end();


            });


        });

    }





//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//            response.write(JSON.stringify({status:10}));
//            response.end();
}

function celShopper(request, response) {
    console.log("this is a test");
    celShop.getAllTestData(function (err, result) {
        console.log(result[0].itemName);
    });

}

function salohinMoTo(request, response) {


    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            //body of the function 
            var post = qs.parse(body);

            //post holds the data from unity
            post.allianceNo;
            //process data

            //send data to unity

            response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
            response.write(JSON.stringify({fasdfasdf: 10, sdfasdfasdfa: 1312312}));
            response.end();
        });
    }



}





function ranncelSettingUpdate(request, response) {


//    ranncelUpdate.settingUpdate(3,"1233", function(err,result){console.log(result);
//    
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetchdata:result}));
//    response.end();
//    
//    
//    });
//    

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            //body of the function 
            var post = qs.parse(body);

            //post holds the data from unity
            post.allianceNo;
            //process data

            //send data to unity

            ranncelUpdate.settingUpdate(post.id, post.newpassword, function (err, result) {
                console.log(result);

                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchdata: result}));
                response.end();


            });
        });
    }

}

function ranncelSettingSelect(request, response) {


//    ranncelUpdate.settingChangePass(2,"123456", function(err,result){console.log(result);
//    
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetchdata:result}));
//    response.end();


//    });

    if (request.method == 'POST') {
        var body = '';

        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            //body of the function 
            var post = qs.parse(body);

            //post holds the data from unity
            post.allianceNo;
            //process data

            //send data to unity

            ranncelUpdate.settingChangePass(post.id, post.oldpassword, function (err, result) {
                console.log(result);

                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchdata: result}));
                response.end();


            });
        });
    }

}


//raymond function coding

function raymondAlliances(request, response){
    
//   AlliancesJs.getdataAlliances(1,function(err,result){console.log(result);
//   response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//   response.write(JSON.stringify({fetcheddata:result}));
//   response.end();
//   });
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceID :"+post.id1);
  AlliancesJs.getdataAlliances(post.id1,function(err,result){console.log(result);
   response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
   response.write(JSON.stringify({fetcheddata:result}));
   response.end();
    });
    
    });
    }
    
    
}


function raymondAlliancesRegUsr(request, response){
    
//    AlliancesJs.getdataRegUser(1, function(err,result){ console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//   response.write(JSON.stringify({fetcheddata:result}));
//   response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceID :"+post.id1);
   AlliancesJs.getdataRegUser(post.id1, function(err,result){ console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
   response.write(JSON.stringify({fetcheddata:result}));
   response.end();
    });
    
    });
    }
 
}

function raymondGetMembrName(request, response){
    
//    AlliancesJs.getMemberName(2 ,function (err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//   response.write(JSON.stringify({fetcheddata:result}));
//   response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceID :"+post.AllianceMembr);
   AlliancesJs.getMemberName(post.AllianceMembr ,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
   response.write(JSON.stringify({fetcheddata:result}));
   response.end();
    });
    
    });
    }
    
    
    
    
}


function raymondInsertDataAlliance(request, response){
   
    
//    AlliancesJs.insertdataAlliance(AllianceName,rank,experience,leader,founder,guidline,alliance_membr_count,function (err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//   response.write(JSON.stringify({fetcheddata:result}));
//   response.end();
//    });
    
      if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceName :"+post.AllianceName);
    AlliancesJs.insertdataAlliance(post.AllianceName,post.rank,post.experience,
    post.leader,post.founder,post.guidline,post.alliance_membr_count,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
   response.write(JSON.stringify({fetcheddata:result}));
   response.end();
    });
    });
    }
    
    
    
    
}



function raymondGetMembrCount(request, response){
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceName :"+post.AllianceName);
    AlliancesJs.AllianceMmberCount(post.AllianceIDnotDestry,function(err,result){console.log(result);
     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
     response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
    });
    }
}

function raymondGetAllianceList(request, response){
    
    
//    AlliancesJs.getDataAllianceList(0,function(err,result){console.log(result);
//     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//});
   
     if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceName :"+post.AllianceID);
   AlliancesJs.getDataAllianceList(post.AllianceID,function(err,result){console.log(result);
     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
});
    });
    }
}


function raymondInsertAllianceReport(request, response){
    
//    AlliancesJs.insertAllianceReport(Message, allianceid, invitation, memberInvolve,memberInvolveid,
//    alliance_leader,alliance_founder,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
 if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("AllianceName :"+post.allianceid);//
            
    AlliancesJs.insertAllianceReport(post.Message,post.allianceid, post.invitation, post.memberInvolve,post.memberInvolveid,
    post.alliance_leader,post.alliance_founder,post.AllianceReport,post.haveAlliance,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
            
            
  //
    });
    }
    
}


function raymondShowReport(request, response){
    
//    AlliancesJs.getAllianceReport(2,1,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
     if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
    AlliancesJs.getAllianceReport(post.Allianceid,post.AllianceReport,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });  
  //
    });
    }
    
}




function raymondShowApplication(request, response){
    
//    AlliancesJs.getApplication(2,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.allianceid);//
    AlliancesJs.getApplication(post.allianceid,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}

function raymondUpdateReguser(request, response){
    
//    AlliancesJs.updateAllianceMembr(0,1, function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
      if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.allianceid);//
    AlliancesJs.updateAllianceMembr(post.allianceid,post.playerID, function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
}


function raymondUpdateMmberCount(request, response){
    
//    AlliancesJs.updateMembrCount(2,1,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.allianceid);//
    AlliancesJs.updateMembrCount(post.membrCount,post.Allianceid,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
  
}


function raymondSelectMembrCount(request, response){
    
//    AlliancesJs.selectMembrCount(1,function (err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.allianceid);//
   AlliancesJs.selectMembrCount(post.allianceid,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    }); 
  //
    });
    }
    
    
    
}




function raymondCheckReport(request, response){
    
//    AlliancesJs.CheckReportexixst(1,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//});


if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
                   
        AlliancesJs.CheckReportexixst(post.memberid,function(err,result){
        console.log(result);

            if(result.length > 0){
                
                console.log("may laman");
                
            }
            else{
                
                console.log("wala laman");
                AlliancesJs.insertAllianceReport(post.Message,post.allianceid, post.invitation, post.memberInvolve,post.memberInvolveid,
                post.alliance_leader,post.alliance_founder,post.AllianceReport,post.haveAlliance,function(err,result){console.log(result);
                console.log(err);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetcheddata:result}));
                response.end();
                });
                
                
            }
    
});
  //
    });
    }
    
}

function raymondNewMembr(request, response){
    
    
//    AlliancesJs.insertAlliancetMembr(2,2,2,2,2,"marco",function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
    request.on('end', function (){
    var post = qs.parse(body);
    AlliancesJs.insertAlliancetMembr(post.alliance,post.member,post.operation_code,post.operation_details,post.status,post.membersname
    ,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });         
        
  //
    });
    }
    
}

function raymondNeutral(request,response){
    
    
//    AlliancesJs.selectAllianceNeutral(2,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
   AlliancesJs.selectAllianceNeutral(post.Allianceid,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}

function raymondInsertEvent(request,response){
    
//    AlliancesJs.insertEvent("name","accepted",2,1,function(err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });

    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
   AlliancesJs.insertEvent(post.members, post.remarks, post.allianceid, post.membersid,post.eventAlliance,function(err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}

function raymondSelectEvent(request,response){
    
//    AlliancesJs.selectEvent(2,function(err,result){console.log(result);
//     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
      if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
  AlliancesJs.selectEvent(post.Allianceid,post.eventAlliance,function(err,result){console.log(result);
     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}


function raymondSelectInvite(request,response){
    
    
//    AlliancesJs.Selectinvite(0,function (err,result){console.log(result);
//     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
    
    
     if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
   AlliancesJs.Selectinvite(post.Allianceid,function (err,result){console.log(result);
     response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}

function raymondSelectApps(request, response){
    
//    AlliancesJs.selectApps(1,function (err,result){console.log(result);
//    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();
//    });
//    
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("Allianceid :"+post.Allianceid);//
  AlliancesJs.selectApps(post.Allianceid,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
    
}


function raymondAllies(request, response){
    
   AlliancesJs.selectAllianceAllies(1,function(err,result){console.log(result);
   response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
   
   }); 
    
    
}

function raymondEnemy(request, response){
    
    AlliancesJs.selectAllianceEnemy(1,function(err,result){console.log(result);
   response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
   
   }); 
    
    
}



function raymondInvitedList(request, response){
    
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("userid :"+post.userNo);//
  AlliancesJs.selectInvitedList(post.userNo,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}


function raymondInsertAllies(request, response){
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("userid :"+post.userNo);//
   AlliancesJs.insertAllies(post.allianceid,post.title,post.rank,post.experience,post.allies,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
    
}


function raymondInsertEnemy(request, response){
    
    if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("userid :"+post.userNo);//
   AlliancesJs.insertEnemy(post.allianceid,post.title,post.rank,post.experience,post.enemy,function (err,result){console.log(result);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();
    });
  //
    });
    }
}


function raymondEventlist(request, response){
    
    
//AlliancesJs.eventmembrlist(1,function(err,result){console.log(result);
//  response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
//    response.write(JSON.stringify({fetcheddata:result}));
//    response.end();   
//});
    

  if(request.method == 'POST'){
        var body = '';
        
        request.on('data', function(data){
            body += data;
            
                if(body.length > 1e6){
                    request.connection.destroy();
                }
            
        } );
        
        request.on('end', function (){
            var post = qs.parse(body);
            console.log("userid :"+post.userNo);//
   AlliancesJs.eventmembrlist(post.Allianceid,function(err,result){console.log(result);
  response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({fetcheddata:result}));
    response.end();   
});
  //
    });
    }
    
}

//end of raymond coding

/*-------------------------RAIN------------------------*/


function rainInsertChat(request, response) {
//    console.log("insert chat was accessed.");
    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            console.log("ALLIANCE MESSAGE : " + post.message);
            console.log("Player Name :" + post.userName);
//          console.log(post.allianceName);
//         rainLocalChat.getAllTestData(1, function (err, result) {
//         console.log(result);
            rainLocalChat.insertAllData(post.userName, post.message, function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedObject: result}));
                response.end();
            });
        });
    }
}

function rainFetchAllChat(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            // console.log("Chat Time : " + post.createdWhen);

            console.log("Created When : " + post.createdWhen);
            console.log("Alliance Name :", post.title);
            console.log("User Rank:" + post.rank_title);
            console.log("User Message :" + post.message);
            rainLocalChat.getAllData("createdWhen", "title", "rank_title", "message", function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedData: result}));
                response.end();
            });
        });
    }
}

function rainGetAllMarket(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            console.log("Player ID : " + post.id);
            console.log("Player Name MARKET: " + post.playerName);
            console.log("Food:", +post.Food);
            console.log("Wood:", +post.Wood);
            console.log("Iron:", +post.Iron);
            console.log("Copper:", +post.Copper);
            console.log("Stone:", +post.Stone);
            console.log("Quantity:", +post.Quantity);
            console.log("UnitPrice:" + post.UnitPrice);
            console.log("Commission :" + post.Commission);
            console.log("DateTimeCreated :" + post.DateTimeCreated);
            rainMarketModule.getAllDataMarket("id", "playerName", "Food", "Wood", "Iron", "Copper", "Stone", "Quantity", "UnitPrice", "Commission", "TotalPrice", "DateTimeCreated", function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedDataMarket: result}));
                response.end();
            });
        });
    }
}

function rainInsertDataMarket(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            // console.log("Chat Time : " + post.createdWhen);
            console.log("Player Name:" + post.playerName);
//            console.log("Food: " + post.Food);
//            console.log("Wood:", post.Wood);
//            console.log("Iron:" + post.Iron);
//            console.log("Copper:" + post.Copper);
//            console.log("Stone: " + post.Stone);
            console.log("Quantity:", +post.Quantity);
            console.log("UnitPrice:", +post.UnitPrice);
            console.log("Commission:", +post.Commission);
            console.log("TotalPrice:", +post.TotalPrice);

            rainMarketModule.insertDataMarket(post.playerName,
                    post.Quantity,
                    post.UnitPrice,
                    post.Commission,
                    post.TotalPrice, function (err, result) {
                        console.log(result);

                        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                        response.write(JSON.stringify({fetchedData: result}));
                        response.end();
                    });
        });
    }
}

function deleteFromMarket(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            console.log("Player ID : " + post.id);

            rainMarketModule.deleteDataFromMarket(post.id, function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedData: result}));
                response.end();
            });
        });
    }
}

function rainInsertDataMarketPersonal(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            // console.log("Chat Time : " + post.createdWhen);
            console.log("Player Name:" + post.Name);
//            console.log("Food: " + post.Food);
//            console.log("Wood:", post.Wood);
//            console.log("Iron:" + post.Iron);
//            console.log("Copper:" + post.Copper);
//            console.log("Stone: " + post.Stone);
            console.log("Quantity:", +post.quantity);
            console.log("UnitPrice:", +post.unitprice);
            console.log("Commission:", +post.commission);
            console.log("TotalPrice:", +post.totalprice);

            rainMarketModule.insertDataMarketPersonal(post.Name, post.quantity, post.unitprice, post.commission, post.totalprice, function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedData: result}));
                response.end();
            });
        });
    }
}

function marketPersonalFetchAll(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            console.log("Personal ID :" + post.id);
            console.log("Personal Name :" + post.Name);
            console.log("Personal Food :" + post.food);
            console.log("Personal wood :" + post.wood);
            console.log("Personal iron :" + post.iron);
            console.log("Personal copper :" + post.copper);
            console.log("Personal stone :" + post.stone);
            console.log("Personal quantity :" + post.quantity);
            console.log("Personal unitprice :" + post.unitprice);
            console.log("Personal commission :" + post.commission);
            console.log("Personal totalprice :" + post.totalprice);
            console.log("Personal date/time :" + post.datetimecreated);


            rainMarketModule.marketPersonalFetchAll(post.id, post.Name, post.food, post.wood, post.iron, post.copper, post.stone, post.quantity, post.unitprice, post.commission, post.totalprice, post.datetimecreated,
                    function (err, result) {
                        console.log(result);
                        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                        response.write(JSON.stringify({fetchedDataMarket: result}));
                        response.end();

                    });
        });
    }
}

function buyMarket(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            // console.log("Chat Time : " + post.createdWhen);
            console.log("Player Name:" + post.Name);
//            console.log("Food: " + post.Food);
//            console.log("Wood:", post.Wood);
//            console.log("Iron:" + post.Iron);
//            console.log("Copper:" + post.Copper);
//            console.log("Stone: " + post.Stone);
            console.log("Quantity:", +post.quantity);
            console.log("UnitPrice:", +post.unitprice);
            console.log("Commission:", +post.commission);
            console.log("TotalPrice:", +post.totalprice);

            rainMarketModule.buyMarket(post.Name,
                    post.quantity,
                    post.unitprice,
                    post.commission,
                    post.totalprice, function (err, result) {
                        console.log(result);

                        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                        response.write(JSON.stringify({fetchedDataBuyMarket: result}));
                        response.end();
                    });
        });
    }
}

function deleteWhenBuy(request, response) {

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            console.log("Player ID : " + post.id);

            rainMarketModule.deleteWhenBuy(post.id, function (err, result) {
                console.log(result);
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({fetchedData: result}));
                response.end();
            });
        });
    }
}

/*-------------------------RAIN END------------------------*/


/* ------------------ ron newly added functions ----------------------------- */

function ronTestInsertReduceResource(request, response) {
    var resourceToReduce = {cityId: 2, food: -1000, wood: -1000, stone: -1000, iron: -1000, copper: -1000, silver: -1000, gold: -1000};
//    play
    dbPlayerInformation.reduceResourcesFromCity(2, resourceToReduce, function (err, result) {
        rr.ReduceResourceSystem.addReduceResourceToUser(2, resourceToReduce);
        response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
        response.end();
    });

}

function ronCheckReduceResource(request, response) {
//    var resourceToReduce = {cityId:, food: 10, wood: 10, stone: 10, iron: 10, copper: 10, silver: 10, gold: 10};
    rr.ReduceResourceSystem.addReduceResourceToUser(2, resourceToReduce);
    console.log('printing resource reduce count:');
    console.log(rr.ReduceResourceSystem.reduceResourceIndex2);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.end();
}


function ronTestAddReport(request, response) {
    var reportItem = ReportItem();
    reportItem.title = "test insert.";
    reportItem.userId = 2;
    reportItem.cityId = 2;
    reportItem.serverInsert = 1;
    reportItem.serverInsertId = Date.now();
    rs.ReportSystem.addUserReport(2, reportItem, function () {});
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.end();
}

function ronGetReduceResourceByUserId(request, response) {
    console.log('Calling ronGetReduceReportByUserId method ---------------------------------2');
    var reduceResource = rr.ReduceResourceSystem.getReduceResourceByUser(2);
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({reduceResource: reduceResource}));
    response.end();
}

function ronGetReportsByUserId(request, response) {
    console.log('Calling ronGetReportsByUserId method ---------------------------------');

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            //var reduceResource = rr.ReduceResourceSystem.getReduceResourceByUser(2);
            rs.ReportSystem.getUserReport(post.userId, function (passedArray) {
                var reportItems = [];
                for (counter = 0; counter < passedArray.length; counter++) {
//                    console.log(passedArray[counter]);
                    reportItems.push(passedArray[counter].reportItemToObj());
                }
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                var reduceResource = rr.ReduceResourceSystem.getReduceResourceByUser(post.userId);
                var reduceUnit = ru.ReduceUnitSystem.getReduceUnitByUser(post.userId);
                var reduceCityStat = rcs.ReduceCityStatSystem.getReduceCityStatByUser(post.userId);
                var destroyCity = dcs.DestroyCitySystem.getDestroyCityByUser(post.userId);
                response.write(JSON.stringify({reports: reportItems, reduceResource: reduceResource, reduceUnit: reduceUnit, reduceCityStat: reduceCityStat, destroyCity: destroyCity}));
                response.end();
            });

        });
    }
}

function ronGetReportScoutUnitByReportId(request, response) {
    console.log('Calling ronGetReportsByUserId method ---------------------------------');

    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });
        request.on('end', function () {
            var post = qs.parse(body);
            //var reduceResource = rr.ReduceResourceSystem.getReduceResourceByUser(2);
            dbReportScoutUnit.getReportScoutUnitByReportId(post.reportId, function (err, result) {
                response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                response.write(JSON.stringify({reportScoutUnits: result}));
                response.end();
            });
        });
    }
}

function getServerTimeStamp(request, response) {
    var now = new Date();
    var serverTimeString = now.getFullYear() + "-" + now.getMonth() + "-" + now.getDate() + " " + now.getUTCHours() + ":" + now.getUTCMinutes() + ":" + getUTCSeconds();
    response.writeHead(200, {'Context-type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
    response.write(JSON.stringify({serverDateTime: serverTimeString}));
    response.end();
}

/* ------------------ end ron newly added functions ----------------------------- */


app.use('/attack', attack);

app.use('/getServerTimeStamp', getServerTimeStamp);
app.use('/checkMarchResult', checkMarchResult);
app.use('/getAllCloseCity', getAllCloseCity);
app.use('/checkMarchResult', checkMarchResult);
app.use('/doRegistration', doRegistration);
app.use('/doLogin', doLogin);
app.use('/trainTroops', trainTroops);


app.use("/destroyDowngradeCityBuilding", destroyDowngradeCityBuilding);
app.use("/destroyDowngradeCityField", destroyDowngradeCityField);

app.use("/createUpgradeCityBuilding", createUpgradeCityBuilding);
app.use("/createUpgradeCityField", createUpgradeCityField);

app.use("/getAllUnitsByUser", getAllUnitsByUser);
app.use("/getAllUnitsFromCity", getAllUnitsFromCity);

app.use("/getAllBuildingsByUser", getAllBuildingsByUser);
app.use("/getAllBuildingsFromCity", getAllBuildingsFromCity);
app.use("/getAllBuildingMatrix", getAllBuildingMatrix);
app.use("/getAllBuildingMatrixUpCost", getAllBuildingMatrixUpCost);
app.use("/getUserCities", getUserCities);
app.use("/getAllUnitMatrix", getAllUnitMatrix);

app.use("/getAllFieldsByUser", getAllFieldsByUser);
app.use("/getAllFieldsFromCity", getAllFieldsFromCity);
app.use("/getAllFieldsMatrix", getAllFieldsMatrix);
app.use("/getAllFieldLevelMatrix", getAllFieldLevelMatrix);
app.use("/getAllFieldMatrixUpCost", getAllFieldMatrixUpCost);

app.use("/getResourcesFromCity", getResourcesFromCity);
app.use("/getAllMatrix", getAllMatrix);

app.use("/getAllTest", getAllTest);


app.use("/celShopper", celShopper);

app.use('/salohinMoTo', salohinMoTo);
app.use('/rainAllianceChat', rainAllianceChat);
app.use('/ranncelSettingUpdate', ranncelSettingUpdate);
app.use('/ranncelSettingSelect', ranncelSettingSelect);

//raymond appuse coding

app.use('/raymondAlliances',raymondAlliances);
app.use('/raymondAlliancesRegUsr',raymondAlliancesRegUsr);
app.use('/raymondGetMembrName', raymondGetMembrName);
app.use('/raymondInsertDataAlliance',raymondInsertDataAlliance);
app.use('/raymondGetMembrCount',raymondGetMembrCount);
app.use('/raymondGetAllianceList',raymondGetAllianceList);
app.use('/raymondInsertAllianceReport',raymondInsertAllianceReport);
app.use('/raymondShowReport',raymondShowReport);
app.use('/raymondShowApplication',raymondShowApplication);
app.use('/raymondUpdateReguser',raymondUpdateReguser);
app.use('/raymondUpdateMmberCount',raymondUpdateMmberCount);
app.use('/raymondSelectMembrCount',raymondSelectMembrCount);
app.use('/raymondCheckReport',raymondCheckReport);
app.use('/raymondNewMembr',raymondNewMembr);
app.use('/raymondNeutral',raymondNeutral);
app.use('/raymondInsertEvent',raymondInsertEvent);
app.use('/raymondSelectEvent',raymondSelectEvent);
app.use('/raymondSelectInvite',raymondSelectInvite);
app.use('/raymondSelectApps',raymondSelectApps);
app.use('/raymondAllies',raymondAllies);
app.use('/raymondEnemy',raymondEnemy);
app.use('/raymondInvitedList',raymondInvitedList);
app.use('/raymondInsertAllies',raymondInsertAllies);
app.use('/raymondInsertEnemy',raymondInsertEnemy);
app.use('/raymondEventlist',raymondEventlist);

/* ---------------start rain functions---------------*/
app.use('/rainInsertChat', rainInsertChat);
app.use('/rainFetchAllChat', rainFetchAllChat);

app.use('/rainGetAllMarket', rainGetAllMarket);
app.use('/rainInsertDataMarket', rainInsertDataMarket);

app.use('/deleteFromMarket', deleteFromMarket);
app.use('/marketPersonalFetchAll', marketPersonalFetchAll);

app.use('/rainInsertDataMarketPersonal', rainInsertDataMarketPersonal);
app.use('/buyMarket', buyMarket);
app.use('/deleteWhenBuy', deleteWhenBuy);
/* ---------------end rain functions---------------*/


/* ------------------- added new ron entry ---------------------- */

app.use('/getCloseEmptyLandCities', getCloseEmptyLandCities);
app.use('/getClosePlayerCities', getClosePlayerCities);
app.use('/getAllCloseBarbarianCity', getAllCloseBarbarianCity);
app.use('/getCloseAllFields', getCloseAllFields);
app.use('/ronGetReportsByUserId', ronGetReportsByUserId);
app.use('/ronGetReduceResourceByUserId', ronGetReduceResourceByUserId);
app.use('/getReportScoutUnitByReportId', ronGetReportScoutUnitByReportId);

app.use('/ronTestInsertReduceResource', ronTestInsertReduceResource);
app.use('/ronCheckReduceResource', ronCheckReduceResource);
app.use('/ronTestAddReport', ronTestAddReport);
/* ------------------- added new ron entry ---------------------- */
