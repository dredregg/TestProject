var __ = require('underscore');
var mysql_connection = require('../mysql_connection');
var dbConnection = mysql_connection.connection();
var dbMultipleConnection = mysql_connection.mconnection();
var unit = require('../unit');


function addUnitToCity(unit, city_id) {

    selectUnitFromCity(unit, city_id, function (err, result) {

        if (result.length > 0) {
            //console.log("was here");
            var criteria = {id: result[0].id};
            var sql = "UPDATE ron_city_units SET unitscount = unitscount + 1 WHERE ?";
            var query = dbConnection.query(sql, criteria, function (err1, result1) {
                //console.log(query.sql);
                if (err1) {
                    console.error(err);
                    return;
                }
            });
        } else {
            var unitToInsert = {cityno: city_id, unitid: unit.id, unit_type: unit.name, unitscount: 1};
            var sql = "INSERT INTO ron_city_units SET ?";
            var query = dbConnection.query(sql, unitToInsert, function (err1, result1) {
                console.log(query.sql);
                if (err1) {
                    console.error(err);
                    return;
                }
            });
        }
    });

}

function selectUnitFromCity(unit, city_id, cb) {
    var sql = "SELECT * FROM ron_city_units WHERE ? AND ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, [{cityno: city_id}, {unit_type: unit.name}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function addMultipleUnitToCity(city_id, unit, unitCount) {
    selectUnitFromCity(unit, city_id, function (err, result) {

        if (result.length > 0) {
            //console.log("was here");
            var criteria = {id: result[0].id};
            var sql = "UPDATE ron_city_units SET unitscount = unitscount + "+unitCount+" WHERE ?";
            var query = dbConnection.query(sql, criteria, function (err1, result1) {
                console.log(query.sql);
                if (err1) {
                    console.error(err);
                    return;
                }
            });
        } else {
            var unitToInsert = {cityno: city_id, unitid: unit.id, unit_type: unit.name, unitscount: unitCount};
            var sql = "INSERT INTO ron_city_units SET ?";
            var query = dbConnection.query(sql, unitToInsert, function (err1, result1) {
                console.log(query.sql);
                if (err1) {
                    console.error(err);
                    return;
                }
            });
        }
    });
}


/**
 * 
 * @param {Matrix.UnitMatrix} unit
 * @param {type} unitCount
 * @param {type} city_id
 * @returns {undefined}
 */
function trainTroops(battleUnit, matrixUnit, unitCount, city_id) {
    var trainingTime = matrixUnit.ltime;//seconds
    var troopsToTrain = unitCount;
    var trainingTroopProcess = setInterval(function () {
        troopsToTrain--;
        if (troopsToTrain == 0) {
            clearInterval(trainingTroopProcess);
        }
        console.log("adding unit to city 2");
        addUnitToCity(battleUnit, city_id);

    }, trainingTime * 1000);
}


/**
 * 
 * @param {type} units element is {unitName: "", unitCount: 12}
 * @returns {undefined}
 */
function killManyTroops(units, cityId, callback) {
    var sql = "";
    __.each(units, function (element, index, list) {
        sql += 'UPDATE ron_city_units SET unitscount = unitscount - ' + element.unitCount + ' WHERE cityno = ' + cityId + ' AND unit_type = "' + element.unitName + '";';
    });


    //dbConnection.connect();
    var query = dbMultipleConnection.query(sql, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}
module.exports.trainTroops = trainTroops;
module.exports.killManyTroops = killManyTroops;
module.exports.addUnitToCity = addUnitToCity;
module.exports.addMultipleUnitToCity = addMultipleUnitToCity;