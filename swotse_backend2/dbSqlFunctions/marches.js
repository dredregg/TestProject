var __ = require('underscore');
var mysql_connection = require('../mysql_connection');
var dbConnection = mysql_connection.connection();

function getMarchResult(march_id, functionToCallAfterQuery) {
    var marchObj = {march_id: march_id}
    var sql = "SELECT * FROM ron_marches_result WHERE ?";
    var query = dbConnection.query(sql, marchObj);

    query.on('error', function (err) {
        throw err;
    });

    query.on('result', function (row) {
        functionToCallAfterQuery(row);
    });
}

function insertMarchResult(march_id, status_id) {
    var dbEntry = {march_id: march_id, status_id: status_id};
    var sql = "INSERT INTO ron_marches_result SET ?";
    var query = dbConnection.query(sql, dbEntry, function (err, result) {
        if (err) {
            console.error(err);
            return;
        }
    });
}

function insertMarches(march_type, knightno, current_city, target_city, object_type, object_id, speed, distance, upkeep, oneway_trip, status, arraival_in_seconds, units, target_city_type, current_city_user_id, target_city_user_id, target_city_is_owned, cb, lastInsertIdCallback) {
    var attackEntry = {
        march_type: march_type,
        knightno: knightno,
        //action_date: 'DATE_ADD(NOW(), INTERVAL 6000 SECONDS)',
        current_city: current_city,
        target_city: target_city,
        object_type: object_type,
        object_id: object_id,
        speed: speed,
        distance: distance,
        upkeep: upkeep,
        oneway_trip: oneway_trip,
        status: status,
        target_city_type: target_city_type,
        current_city_user_id: current_city_user_id,
        target_city_user_id: target_city_user_id,
        target_city_is_owned: target_city_is_owned,
    };

    var sql = "INSERT INTO ron_marches SET ? , action_date = DATE_ADD(NOW(), INTERVAL " + arraival_in_seconds + " SECOND)";
    //dbConnection.connect();
    var query = dbConnection.query(sql, attackEntry, function (err, result) {
        //console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        __.each(units, function (element, index, list) {
            inserMarchesUnit(result.insertId, element.unitId, element.unitCount, element.unitType);
        });
        lastInsertIdCallback(result.insertId);
        setTimeout(function () {
            cb(result.insertId);//calls the main.js doAttack
        }, arraival_in_seconds * 1000);
    });
    //dbConnection.end();

}

function inserMarchesUnit(march_id, unit_id, unit_count, unit_type) {
    var sql = "INSERT INTO ron_marches_units SET ?";
    var unitObj = {
        march_id: march_id, unitid: unit_id, unitcount: unit_count, unit_type: unit_type
    };
    var query = dbConnection.query(sql, unitObj, function (err, result) {
        //console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
    });
}

function getMarchById(marchId, functionToCallAfterQuery) {
    var sql = "SELECT * FROM ron_marches WHERE ?";

    var marchIdObj = {id: marchId};
    //dbConnection.connect();
    var query = dbConnection.query(sql, marchIdObj);

    query.on('error', function (err) {
        throw err;
    });

    query.on('result', function (row) {
        functionToCallAfterQuery(row);
    });

}

function getMarchUnitByMarchId(marchId, functionToCallAfterQuery) {
    var sql = "SELECT * FROM ron_marches_units WHERE ?";

    var marchIdObj = {march_id: marchId};
    var marchUnits = {};
    //dbConnection.connect();
    var query = dbConnection.query(sql, marchIdObj, function (err, result) {
        functionToCallAfterQuery(err, result);
    });


}

function getUnitsFromCity(cityId, cb) {
    var criteria = {cityno: cityId};
    var sql = "SELECT * FROM ron_city_units WHERE ?";
    var query = dbConnection.query(sql, criteria, function (err, result) {
        cb(err, result);
    });
}

module.exports.insertMarches = insertMarches;
module.exports.getMarchById = getMarchById;
module.exports.getMarchUnitByMarchId = getMarchUnitByMarchId;
module.exports.getUnitsFromCity = getUnitsFromCity;
module.exports.insertMarchResult = insertMarchResult;
module.exports.getMarchResult = getMarchResult;