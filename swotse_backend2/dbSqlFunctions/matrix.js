var mysql_connection = require('../mysql_connection');
var unit = require('../unit');
var dbConnection = mysql_connection.connection();

var unitMatrix = {};
var battleUnits = {};
var buildingMatrix = {};
var buildingMatrixUpCost = {};
var buildingMatrixDownCost = {};
var fieldsMatrix = {};
var fieldsLevelMatrix = {};
var fieldsMatrixUpCosts = {};
var fieldsMatrixDownCosts = {};

var languagePackEn = {};

function initializeMatrix(initializationCallBack) {

    var sql = "SELECT * FROM ron_units_matrix WHERE name <> ''";
    //dbConnection.connect();
    var query = dbConnection.query(sql);

    var unitIsDone = false;
    var fieldsMatrixIsDone = false;
    var fieldsLevelMatrixIsDone = false;
    var fieldsMatrixUpCostIsDone = false;
    var buildingMatrixIsDone = false;
    var buildingMatrixUpCostIsDone = false;
    var buildingMatrixDownCostIsDone = false;
    var fieldsMatrixDownCostIsDone = false;
    var languagePackIsDone = false;

    query.on('error', function (err) {
        throw err;
    });

    query.on('result', function (row) {
        battleUnits[row.name] = unit(row.speed, row.life, row.attack, row.defence, row.attack_range, row.name, row.take_damage_order, row.id, row.capacity, row.population);
        unitMatrix[row.name] = row;

    });

    query.on('end', function () {
        unitIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllLanguagePackEn(function(err, result){
        languagePackEn["result"] = result;
        languagePackIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    //dbConnection.end();
    getAllFieldsMatrix(function (err, result) {
        fieldsMatrix["result"] = result;
        fieldsMatrixIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllFieldLevelMatrix(function (err, result) {
        fieldsLevelMatrix["result"] = result;
        fieldsLevelMatrixIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllFieldMatrixUpCost(function (err, result) {
        fieldsMatrixUpCosts["result"] = result;
        fieldsMatrixUpCostIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllFieldMatrixDownCost(function (err, result) {
        fieldsMatrixDownCosts["result"] = result;
        fieldsMatrixDownCostIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllBuldingMatrix(function (err, result) {
        buildingMatrix["result"] = result;
        buildingMatrixIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllBuildingMatrixUpCost(function (err, result) {
        buildingMatrixUpCost["result"] = result;
        buildingMatrixUpCostIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });

    getAllBuildingMatrixDownCost(function (err, result) {
        buildingMatrixDownCost["result"] = result;
        buildingMatrixDownCostIsDone = true;
        initializationCallBack(
                unitIsDone && fieldsMatrixIsDone &&
                fieldsLevelMatrixIsDone && fieldsMatrixUpCostIsDone &&
                buildingMatrixIsDone && buildingMatrixUpCostIsDone &&
                buildingMatrixDownCostIsDone && fieldsMatrixDownCostIsDone &&
                languagePackIsDone
                );
    });
}

function getAllLanguagePackEn(cb) {
    var sql = "SELECT * FROM ron_lang_en";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldsMatrix(cb) {
    var sql = "SELECT * FROM ron_field_bldg_matrix";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldLevelMatrix(cb) {
    var sql = "SELECT * FROM ron_field_bldg_level_matrix";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldMatrixUpCost(cb) {
    var sql = "SELECT * FROM ron_field_matrix_up_cost";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldMatrixDownCost(cb) {
    var sql = "SELECT * FROM ron_field_matrix_down_cost";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllBuldingMatrix(cb) {
    var sql = "SELECT * FROM ron_bldg_matrix";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllBuildingMatrixUpCost(cb) {
    var sql = "SELECT * FROM ron_bldg_matrix_up_cost";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllBuildingMatrixDownCost(cb) {
    var sql = "SELECT * FROM ron_bldg_matrix_down_cost";

    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


module.exports.battleUnits = battleUnits;
module.exports.languagePackEn = languagePackEn;
module.exports.unitMatrix = unitMatrix;
module.exports.buildingMatrix = buildingMatrix;
module.exports.buildingMatrixUpCost = buildingMatrixUpCost;
module.exports.buildingMatrixDownCost = buildingMatrixDownCost;
module.exports.fieldsMatrix = fieldsMatrix;
module.exports.fieldsLevelMatrix = fieldsLevelMatrix;
module.exports.fieldsMatrixUpCosts = fieldsMatrixUpCosts;
module.exports.fieldsMatrixDownCosts = fieldsMatrixDownCosts;

module.exports.initializeMatrix = initializeMatrix;

//module.exports.getAllBuldingMatrix = getAllBuldingMatrix;
//module.exports.getAllBuildingMatrixUpCost = getAllBuildingMatrixUpCost;
//module.exports.getAllFieldsMatrix = getAllFieldsMatrix;
//module.exports.getAllFieldLevelMatrix = getAllFieldLevelMatrix;
//module.exports.getAllFieldMatrixUpCost = getAllFieldMatrixUpCost;