var __ = require('underscore');
var mysql_connection = require('../../../mysql_connection');
var dbConnection = mysql_connection.connection();

var knightCount = 3;//maintain this knight count

function hireKnight(knightId, userId, cityId, callback) {
    var sql = "UPDATE ron_knight_bank SET ? WHERE id = " + knightId + ";";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {city_id: cityId, user_id: userId, is_hired: 1}, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

function insertRandomKnights(knightNo, callback) {
    console.log("Hiring knights");
    var sql = "INSERT INTO ron_knight_bank (title,is_hired, user_id, city_id) SELECT name, 0 , 0 ,0  FROM knights ORDER BY RAND() LIMIT " + knightNo;
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

function removeLastInsertedKnights(knightNo, callback){
    
    console.log("removing previously added knights");
    var sql = "DELETE FROM ron_knight_bank ORDER BY id DESC LIMIT " + knightNo;
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

function maintainKnightList() {
    console.log("Maintaining knight list");
    var sql = "SELECT count(id) as knightCount FROM `ron_knight_bank` WHERE is_hired = 0;";
    //dbConnection.connect();
    var query = dbConnection.query(sql, function (err, result) {
        var knightToHire = knightCount - result[0].knightCount;

        if (knightToHire > 0) {
            insertRandomKnights(knightToHire);
        }else if(knightToHire < 0){
            //negative values
            var knightsToRemove = Math.abs(knightToHire);//will get negative values
            removeLastInsertedKnights(knightsToRemove);
        }
    });
}

module.exports.hireKnight = hireKnight;

maintainKnightList();