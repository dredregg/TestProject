function ReduceResourceSystem() {
    this.userReduceResourceIndexKey = "reduceResourceIndex";

    this.addReduceResourceToUser = function (userId, ResourceObj) {
        var rrIndex = this.userReduceResourceIndexKey;
        if (ReduceResourceSystem.hasOwnProperty(rrIndex + userId)) {
            ReduceResourceSystem[rrIndex + userId].push(ResourceObj);
        } else {
            ReduceResourceSystem[rrIndex + userId] = [];
            ReduceResourceSystem[rrIndex + userId].push(ResourceObj);
        }
    };
    
    this.getReduceResourceByUser = function getReduceResourceByUser(userId){
        var rrIndex = this.userReduceResourceIndexKey;
        if(!ReduceResourceSystem.hasOwnProperty(rrIndex + userId)){
            ReduceResourceSystem[rrIndex + userId] = [];
        }
        var returnObj = ReduceResourceSystem[rrIndex + userId];
        ReduceResourceSystem[rrIndex + userId] = [];
        return returnObj;
    };
}
var reduceResourceInstance = new ReduceResourceSystem();
module.exports.ReduceResourceSystem = reduceResourceInstance;