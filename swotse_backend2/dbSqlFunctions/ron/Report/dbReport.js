var __ = require('underscore');
var mysql_connection = require('../../../mysql_connection');
var dbConnection = mysql_connection.connection();


function getAllReportByUserId(userId, callback) {
    var sql = "SELECT ron_report.*, COALESCE(TIMESTAMPDIFF(SECOND,NOW(),ron_marches.action_date),0) as timer_value FROM ron_report LEFT JOIN ron_marches ON ron_report.march_id = ron_marches.id WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {user_id: userId}, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });

}

function insertReport(reportItem, callback){
    var sql = "INSERT INTO ron_report SET ?";
    var query = dbConnection.query(sql, reportItem.reportItemToDb(), function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

module.exports.getAllReportByUserId = getAllReportByUserId;
module.exports.insertReport = insertReport;