var __ = require('underscore');
var ReportItem = require('./ReportItem');

var reportDb = require('./dbReport');

function ReportSystem() {
    this.userReportIndexKey = "userReport";
    this.addUserReport = function addUserReport(userId, reportItem, callback) {
        var userReportKey = this.userReportIndexKey;
        if (reportItem.reportType == 20) {
            // spy needed a real report item key
            reportDb.insertReport(reportItem, function (err, result) {
                reportItem.id = result.insertId;
                reportItem.serverInsert = 0;
                ReportSystem[userReportKey + userId].push(reportItem);
                callback(err, result);
            });
        } else {
            if (ReportSystem.hasOwnProperty(userReportKey + userId)) {
                ReportSystem[userReportKey + userId].push(reportItem);
            } else {
                //not yet in the list
                ReportSystem[userReportKey + userId] = [];
                this.initializeUserReports(userId, ReportSystem[userReportKey + userId], function () {
                    ReportSystem[userReportKey + userId].push(reportItem);
                });
            }
            reportDb.insertReport(reportItem, function (err, result) {
                callback(err, result);
            });
        }


    };

    this.containsUserReport = function containsUserReport(userId) {
        return this.hasOwnProperty(this.userReportIndexKey + userId);
    };

    this.getUserReport = function getUserReport(userId, callbackAfterFetchingResult) {
        var userReportIndexKey = this.userReportIndexKey;
        console.log('get from server?' + ReportSystem.hasOwnProperty(userReportIndexKey + userId));
        if (ReportSystem.hasOwnProperty(userReportIndexKey + userId)) {
            callbackAfterFetchingResult(ReportSystem[userReportIndexKey + userId]);
        } else {
            //get all the reports from the database
            ReportSystem[this.userReportIndexKey + userId] = [];
            this.initializeUserReports(userId, ReportSystem[this.userReportIndexKey + userId], function () {
                callbackAfterFetchingResult(ReportSystem[userReportIndexKey + userId]);
            });
        }
    };

    this.initializeUserReports = function initializeUserReports(userId, userReportArray, cb) {
        console.log("Initializing user report records");
        reportDb.getAllReportByUserId(userId, function (err, result) {
            //insert all rows to the proper user
            for (ctr = 0; ctr < result.length; ctr++) {
                var reportItem = ReportItem();
                reportItem.init(result[ctr]);
                userReportArray.push(reportItem);
            }
            cb();
        });
    };
}

reportSystem = new ReportSystem();
//reportSystem.getUserReport(2, function(passedArray){
//    console.log(passedArray);
//});
//var reportToAdd = new ReportItem();
//reportToAdd.userId = 4;
//reportToAdd.cityId = 2;
//reportToAdd.title = "test";
//reportToAdd.reportType = 10;
//
//reportSystem.addUserReport(4, reportToAdd);

module.exports.ReportSystem = reportSystem;