module.exports = function () {
    return {
        id: 1,
        userId: 1,
        cityId: 1,
        title: "",
        reportType: 10,
        marchId: 1,
        timerValue: 0,
        serverInsert: 0,
        serverInsertId: 0,
        createdWhen: "test date",
        init: function init(objFromDatabase) {
            this.id = objFromDatabase.id;
            this.userId = objFromDatabase.user_id;
            this.cityId = objFromDatabase.city_id;
            this.title = objFromDatabase.title;
            this.reportType = objFromDatabase.report_type;
            this.marchId = objFromDatabase.march_id;
            this.timerValue = objFromDatabase.timer_value,
            this.createdWhen = objFromDatabase.created_when;
        },
        reportItemToDb: function () {
            return {
                user_id: this.userId,
                city_id: this.cityId,
                title: this.title,
                report_type: this.reportType,
//                timer_value: this.timerValue,
                march_id: this.marchId,
            };
        },
        reportItemToObj: function () {
            return {
                id: this.serverInsert == 0 ? this.id : this.serverInsertId,
                userId: this.userId,
                cityId: this.cityId,
                title: this.title,
                reportType: this.reportType,
                marchId: this.marchId,
                timerValue: this.timerValue,
                createdWhen: this.createdWhen,
            };
        }
    };
};