var __ = require('underscore');
var mysql_connection = require('../../../mysql_connection');
var dbConnection = mysql_connection.connection();



function insertReportScoutUnitMultiple(reportId, cityId, unitList, callback) {
    var sqlEntry = [];
    __.each(unitList, function (element, index, list) {
        sqlEntry.push("(" + reportId + "," + cityId + "," + element.unit.id + "," + element.unitCount + ",\"" + element.unit.name + "\")");
    });

    var sql = "INSERT INTO ron_report_scout_unit (report_id, city_id, unit_id, unit_count, unit_name)  VALUES " + sqlEntry.join(",");
    console.log(sql);
    var query = dbConnection.query(sql, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

function getReportScoutUnitByReportId(reportId, callback) {
    var sql = "SELECT * FROM ron_report_scout_unit WHERE ?";
    console.log(sql);
    var query = dbConnection.query(sql, {report_id: reportId}, function (err, result) {
        console.log(query.sql);
        callback(err, result);
    });
}

module.exports.insertReportScoutUnitMultiple = insertReportScoutUnitMultiple;
module.exports.getReportScoutUnitByReportId = getReportScoutUnitByReportId;