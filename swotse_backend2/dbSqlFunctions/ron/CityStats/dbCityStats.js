var __ = require('underscore');
var mysql_connection = require('../../../mysql_connection');
var dbConnection = mysql_connection.connection();


/**
 * 
 * @param {type} cityId
 * @param {type} statCost points always positive
 * @returns {undefined}
 */
function reduceCityStat(cityId, statCost,cb) {
    //courage, happiness, points
    var sql = "UPDATE user_cities SET courage = courage - (courage * 0." + Math.abs(statCost.courage) + ") ,happiness = happiness - (happiness * 0." + Math.abs(statCost.happiness) + "), points = points + " + Math.abs(statCost.points) + " WHERE ?";
    var query = dbConnection.query(sql, [{cityno: cityId}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function increaseCityStat(cityId, statCost,cb) {
    //courage, happiness, points
    var sql = "UPDATE user_cities SET courage = courage + (courage * 0." + Math.abs(statCost.courage) + ") ,happiness = happiness + (happiness * 0." + Math.abs(statCost.happiness) + "), points = points + " + statCost.points + " WHERE ?";
    var query = dbConnection.query(sql, [{cityno: cityId}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

module.exports.reduceCityStat = reduceCityStat;
module.exports.increaseCityStat = increaseCityStat;