function ReduceCityStatSystem() {
    this.userReduceCityStatIndexKey = "reduceCityStatIndex";

    this.addReduceCityStatToUser = function (userId, CityStatObj) {
        var rcsIndex = this.userReduceCityStatIndexKey;
        if (ReduceCityStatSystem.hasOwnProperty(rcsIndex + userId)) {
            ReduceCityStatSystem[rcsIndex + userId].push(CityStatObj);
        } else {
            ReduceCityStatSystem[rcsIndex + userId] = [];
            ReduceCityStatSystem[rcsIndex + userId].push(CityStatObj);
        }
    };
    
    this.getReduceCityStatByUser = function getReduceCityStatByUser(userId){
        var rcsIndex = this.userReduceCityStatIndexKey;
        if(!ReduceCityStatSystem.hasOwnProperty(rcsIndex + userId)){
            ReduceCityStatSystem[rcsIndex + userId] = [];
        }
        var returnObj = ReduceCityStatSystem[rcsIndex + userId];
        ReduceCityStatSystem[rcsIndex + userId] = [];
        return returnObj;
    };
}
var reduceCityStatInstance = new ReduceCityStatSystem();
module.exports.ReduceCityStatSystem = reduceCityStatInstance;