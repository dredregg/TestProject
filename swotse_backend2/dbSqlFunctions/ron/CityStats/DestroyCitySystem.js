function DestroyCitySystem() {
    this.userDestroyCityIndexKey = "reduceCityStatIndex";

    this.addDestroyCityToUser = function (userId, CityStatObj) {
        var dcIndex = this.userDestroyCityIndexKey;
        if (DestroyCitySystem.hasOwnProperty(dcIndex + userId)) {
            DestroyCitySystem[dcIndex + userId].push(CityStatObj);
        } else {
            DestroyCitySystem[dcIndex + userId] = [];
            DestroyCitySystem[dcIndex + userId].push(CityStatObj);
        }
    };
    
    this.getDestroyCityByUser = function getDestroyCityByUser(userId){
        var dcIndex = this.userDestroyCityIndexKey;
        if(!DestroyCitySystem.hasOwnProperty(dcIndex + userId)){
            DestroyCitySystem[dcIndex + userId] = [];
        }
        var returnObj = DestroyCitySystem[dcIndex + userId];
        DestroyCitySystem[dcIndex + userId] = [];
        return returnObj;
    };
}
var reduceCityStatInstance = new DestroyCitySystem();
module.exports.DestroyCitySystem = reduceCityStatInstance;