var __ = require('underscore');
var mysql_connection = require('../../../mysql_connection');
var dbConnection = mysql_connection.connection();

function getAllCloseEmptyLand(cxpcos, cypos, cb) {
    var sql = "SELECT user_cities.*, ROUND(SQRT(POWER(xpos -  " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` WHERE typecity = 0 ORDER BY distance ASC LIMIT 250";
//    console.log(query.sql);
    var query = dbConnection.query(sql, function (err, result) {
        cb(err, result);
    });
}

function getAllCloseBarbarianCity(cxpcos, cypos, cb) {
//    var sql = "SELECT reg_users.full_name as user_full_name,user_cities.*, ROUND(SQRT(POWER(xpos - " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` INNER JOIN reg_users ON user_cities.userno = reg_users.code WHERE cityno != " + city_id + " ORDER BY distance ASC";
    var sql = "SELECT user_cities.*, ROUND(SQRT(POWER(xpos -  " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` WHERE typecity = 10 ORDER BY distance ASC LIMIT 250";
//    console.log(query.sql);
    var query = dbConnection.query(sql, function (err, result) {
        cb(err, result);
    });
}

function getCloseAllFields(userId, cxpcos, cypos, cb) {
    var sql = "SELECT user_cities.*, ROUND(SQRT(POWER(xpos -  " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` WHERE userno != " + userId + " AND typecity = 20 ORDER BY distance ASC LIMIT 250";
    var query = dbConnection.query(sql, function (err, result) {
//        console.log(query.sql);
        cb(err, result);
    });
}

function getClosePlayersCity(userId, cxpcos, cypos, cb) {
    var sql = "SELECT user_cities.*, ROUND(SQRT(POWER(xpos -  " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` WHERE userno != " + userId + " AND typecity = 1 ORDER BY distance ASC LIMIT 250";
    var query = dbConnection.query(sql, function (err, result) {
//        console.log(query.sql);
        cb(err, result);
    });
}

function getCloseEmptyLandCity(userId, cxpcos, cypos, cb) {
    var sql = "SELECT user_cities.*, ROUND(SQRT(POWER(xpos -  " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` WHERE userno != " + userId + " AND is_owned = 0 AND typecity = 0 ORDER BY distance ASC LIMIT 250";
    var query = dbConnection.query(sql, function (err, result) {
//        console.log(query.sql);
        cb(err, result);
    });
}

module.exports.getAllCloseBarbarianCity = getAllCloseBarbarianCity;
module.exports.getCloseAllFields = getCloseAllFields;
module.exports.getClosePlayersCity = getClosePlayersCity;
module.exports.getCloseEmptyLandCity = getCloseEmptyLandCity;