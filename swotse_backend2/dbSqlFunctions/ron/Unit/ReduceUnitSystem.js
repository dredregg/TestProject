function ReduceUnitSystem() {
    this.userReduceUnitIndexKey = "reduceUnitIndex";

    this.addReduceUnitToUser = function (userId, UnitObj) {
        var ruIndex = this.userReduceUnitIndexKey;
        if (ReduceUnitSystem.hasOwnProperty(ruIndex + userId)) {
            ReduceUnitSystem[ruIndex + userId].push(UnitObj);
        } else {
            ReduceUnitSystem[ruIndex + userId] = [];
            ReduceUnitSystem[ruIndex + userId].push(UnitObj);
        }
    };
    
    this.getReduceUnitByUser = function getReduceUnitByUser(userId){
        var ruIndex = this.userReduceUnitIndexKey;
        if(!ReduceUnitSystem.hasOwnProperty(ruIndex + userId)){
            ReduceUnitSystem[ruIndex + userId] = [];
        }
        var returnObj = ReduceUnitSystem[ruIndex + userId];
        ReduceUnitSystem[ruIndex + userId] = [];
        return returnObj;
    };
}
var reduceUnitInstance = new ReduceUnitSystem();
module.exports.ReduceUnitSystem = reduceUnitInstance;