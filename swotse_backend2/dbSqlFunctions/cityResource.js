var __ = require('underscore');
var mysql_connection = require('../mysql_connection');
var dbConnection = mysql_connection.connection();
var unit = require('../unit');



function reduceCityResource(cityId, resourceCost) {
    var sql = "UPDATE user_cities SET level = level + 1 , ? WHERE ?";
    var query = dbConnection.query(sql, [{status: status}, {id: id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function reduceCityResourceMany(cityId, resourceCosts, cb) {
    var sql1 = "? WHERE ?";
    var sql = "UPDATE user_cities SET ";

    var resourceCost = {food: 0, wood: 0, stone: 0, iron: 0, copper: 0, silver: 0, gold: 0};
    __.each(resourceCosts, function (element1, index1, list1) {
        __.each(Object.keys(element1), function (element, index, list) {
            resourceCost[element] += element1[element];

        });
        //cityUnit.trainTroops(dbMatrix.battleUnits[element.unitType], dbMatrix.unitMatrix[element.unitType], element.unitCount, 2);
    });
    //reduce the column from database, if element is 0 dont touch the column
    var databaseColumnReductionArray = [];
    __.each(Object.keys(resourceCost), function(element, index, list){
        if(resourceCost[element] != 0){
             databaseColumnReductionArray.push(element+"_value = (TIMESTAMPDIFF(SECOND,"+element+"_last_access,now()) * "+element+"_rate) + "+element+"_value - "+ resourceCost[element]+" , "+element+"_last_access = NOW()");
        }
    });
    sql += databaseColumnReductionArray.join(" , ");
    sql += " WHERE ?";

    var query = dbConnection.query(sql, {cityno: cityId}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

module.exports.reduceCityResourceMany = reduceCityResourceMany;