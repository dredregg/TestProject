var mysql_connection = require('../../mysql_connection');
var dbConnection = mysql_connection.connection();


function getAllDataMarket(id,PlayerName,Food,Wood,Iron,Copper,Stone,Quantity,UnitPrice,Commission,TotalPrice,DateTimeCreated,callback){
   var sql = 'SELECT * FROM rain_market_sell';
    var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });  
}

function marketPersonalFetchAll(id,Name,food,wood,iron,copper,stone,quantity,unitprice,commission,totalprice,datetimecreated,callback){
    var sql = 'SELECT * from rain_market_personal';
    var query = dbConnection.query(sql, function(err,result){ 
        callback(err,result);
        
   });
}

function insertDataMarket(playerName,Quantity,UnitPrice,Commission,TotalPrice,callback) {
    var sql = 'INSERT INTO rain_market_sell(`playerName`,`Quantity`, `UnitPrice`, `Commission`, `TotalPrice`) VALUES ("'+playerName+'","'+Quantity+'", "'+UnitPrice+'","'+Commission+'", "'+TotalPrice+'")';

    var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });
}

function insertDataMarketPersonal(Name,quantity,unitprice,commission,totalprice,callback) {
    var sql = 'INSERT INTO rain_market_personal(`Name`,`quantity`, `unitprice`, `commission`, `totalprice`) VALUES ("'+Name+'","'+quantity+'", "'+unitprice+'","'+commission+'", "'+totalprice+'")';

    var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });
}

function deleteDataFromMarket(id,callback){
    var sql ='DELETE FROM rain_market_personal WHERE id = "'+id+'"';
     var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });
}


function buyMarket(Name,quantity,unitprice,commission,totalprice,callback) {
    var sql = 'INSERT INTO rain_market_personal(`Name`,`quantity`, `unitprice`, `commission`, `totalprice`) VALUES ("'+Name+'","'+quantity+'", "'+unitprice+'","'+commission+'", "'+totalprice+'")';

    var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });
}
function deleteWhenBuy(id,callback){
    var sql ='DELETE FROM rain_market_sell WHERE id = "'+id+'"';
     var query = dbConnection.query(sql, function (err, result) {
        callback(err, result);
    });
}


module.exports.insertDataMarket = insertDataMarket;
module.exports.getAllDataMarket=getAllDataMarket;
module.exports.insertDataMarketPersonal=insertDataMarketPersonal;

module.exports.deleteDataFromMarket = deleteDataFromMarket;
module.exports.marketPersonalFetchAll = marketPersonalFetchAll;
module.exports.buyMarket = buyMarket;
module.exports.deleteWhenBuy = deleteWhenBuy;

       
       
       
