var __ = require('underscore');
var crypto = require('crypto');
var mysql_connection = require('../mysql_connection');
var dbConnection = mysql_connection.connection();


function getAllCloseCity(city_id, cxpcos, cypos, cb) {
    var sql = "SELECT reg_users.full_name as user_full_name,user_cities.*, ROUND(SQRT(POWER(xpos - " + cxpcos + ",2) + POWER(ypos - " + cypos + ", 2)),2) as distance FROM `user_cities` INNER JOIN reg_users ON user_cities.userno = reg_users.code WHERE cityno != " + city_id + " ORDER BY distance ASC";
    var query = dbConnection.query(sql, function (err, result) {
        cb(err, result);
    });
}

function registrateUser(username, full_name, password, email, emperor, userLanguage, user_code, points, status, cb) {
    var hash = crypto.createHash('md5').update(password).digest('hex');
    var user  = {title: username, password: hash, email:email, language:userLanguage, full_name:full_name, user_code:user_code, points:points, status:status};
    var query = dbConnection.query('INSERT INTO reg_users SET ?', user, function(err, result) {
        console.log(err);
        cb(err, result);
});
console.log(query.sql);
}

function getUser(username, password, cb) {
    var sql = "SELECT * FROM `reg_users` WHERE ? AND ?";
    var hash = crypto.createHash('md5').update(password).digest('hex');
    var query = dbConnection.query(sql, [{title: username}, {password: hash}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


function getAllUnitsFromCity(city_id, cb) {
    var sql = "SELECT * FROM ron_city_units WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {cityno: city_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllUnitsByUser(user_id, cb) {

    var sql = "SELECT * FROM ron_city_units as RCU INNER JOIN user_cities AS UC ON UC.cityno = RCU.cityno WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {userno: user_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });


}

function getAllBuildingsByUser(user_id, cb) {

    var sql = "SELECT RCB.id, RCB.city_id, RCB.construction_pit, RCB.bldg_matrix_id, RCB.bldg_id, RCB.level, "+
                    "RCB.status,  RCB.to_level, "+
                    "TIMESTAMPDIFF(SECOND,NOW(),completion_time) as completion_time "+
            "FROM ron_city_buildings AS RCB INNER JOIN user_cities UC ON UC.cityno = RCB.city_id WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {userno: user_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllBuildingsFromCity(city_id, cb) {
    var sql = "SELECT *,TIMESTAMPDIFF(SECOND,NOW(),completion_time) as completion_time FROM ron_city_buildings WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {city_id: city_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldsByUser(user_id, cb) {
    var sql = "SELECT RFB.id, RFB.city_id, RFB.construction_pit, RFB.field_id, RFB.field_building_lvl_id, RFB.to_level, "+
                "RFB.status, "+
                "TIMESTAMPDIFF(SECOND,NOW(),completion_time) as completion_time "+
            "FROM ron_field_buildings AS RFB INNER JOIN user_cities UC ON UC.cityno = RFB.city_id  WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {userno: user_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getAllFieldsFromCity(city_id, cb) {
    var sql = "SELECT * FROM ron_field_buildings WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {city_id: city_id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

/****************** fields ********************************/

function downgradeCityField(id, status, fieldLevelBuildingMatrix, cb) {
    var sql = "UPDATE ron_field_buildings SET field_building_lvl_id = " + fieldLevelBuildingMatrix.id + " , ? WHERE ?";
    var query = dbConnection.query(sql, [{status: status}, {id: id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


function upgradeCityField(id, status, fieldLevelBuildingMatrix, cb) {
    var sql = "UPDATE ron_field_buildings SET field_building_lvl_id = " + fieldLevelBuildingMatrix.id + " , ? WHERE ?";
    var query = dbConnection.query(sql, [{status: status}, {id: id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}
/**
 * 
 * @param {type} cityField
 * @param {int} statusPreset 10 for building 0 for idle
 * @param {type} cb
 * @returns {undefined}
 */
function createCityField(cityField, statusPreset, timeCompletion, cb) {

    var cityToInsert = {
        city_id: cityField.cityId, construction_pit: cityField.constructionPitId,
        field_id: cityField.fieldMatrix.id, field_building_lvl_id: cityField.fieldLevelMatrix.id, status: 10, to_level: 1
    };
    var sql = "INSERT INTO ron_field_buildings SET ?, completion_time = DATE_ADD(NOW(), INTERVAL " + timeCompletion + " SECOND)";
    var query = dbConnection.query(sql, cityToInsert, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function destroyCityField(cityField, cb) {
    var sql = "DELETE FROM ron_field_buildings WHERE ?";
    var query = dbConnection.query(sql, {id: cityField.id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });

}

function setCityFieldStatus(fieldBuilding, status, timeCompletion, fieldLevelMatrix, cb) {
    var completion = status == 10 ? " completion_time = DATE_ADD(NOW(), INTERVAL " + timeCompletion + " SECOND)" : '';
    var sql = "UPDATE ron_field_buildings SET ?, " + completion + " WHERE ?";
    var query = dbConnection.query(sql, [{status: status, to_level: fieldLevelMatrix.level}, {id: fieldBuilding.id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


/****************** end fields ********************************/
function downgradeCityBuilding(id, status, cb) {
    var sql = "UPDATE ron_city_buildings SET level = level - 1 , ? WHERE ?";
    var query = dbConnection.query(sql, [{status: status}, {id: id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function destroyCityBuilding(id, cb) {
    var sql = "DELETE FROM ron_city_buildings WHERE ?";
    var query = dbConnection.query(sql, {id: id}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


function upgradeCityBuilding(id, status, cb) {
    var sql = "UPDATE ron_city_buildings SET level = level + 1 , ? WHERE ?";
    var query = dbConnection.query(sql, [{status: status}, {id: id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}
/**
 * 
 * @param {type} cityBuilding
 * @param {int} statusPreset 10 for building 0 for idle
 * @param {type} cb
 * @returns {undefined}
 */
function createCityBuilding(cityBuilding, statusPreset, timeCompletion, cb) {

    var cityToInsert = {
        city_id: cityBuilding.cityId, construction_pit: cityBuilding.constructionPitId,
        bldg_matrix_id: cityBuilding.buildingMatrix.id, bldg_id: cityBuilding.buildingMatrix.buildingId, level: cityBuilding.level, to_level: cityBuilding.toLevel, status: statusPreset
    };
    var sql = "INSERT INTO ron_city_buildings SET ?, completion_time = DATE_ADD(NOW(), INTERVAL " + timeCompletion + " SECOND)";
    var query = dbConnection.query(sql, cityToInsert, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function setCityBuildingStatus(cityBuilding, status, timeCompletion, cb) {
    var completion = status == 10 ? " completion_time = DATE_ADD(NOW(), INTERVAL " + timeCompletion + " SECOND)" : '';
    var sql = "UPDATE ron_city_buildings SET ?, " + completion + " WHERE ?";
    var query = dbConnection.query(sql, [{status: status,to_level: cityBuilding.toLevel}, {id: cityBuilding.id}], function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getUserCities(userId, cb) {
    var sql = "SELECT *," +
            "(TIMESTAMPDIFF(SECOND,food_last_access,now()) * food_rate) + food_value AS food," +
            "(TIMESTAMPDIFF(SECOND,wood_last_access,now()) * wood_rate) + wood_value AS wood," +
            "(TIMESTAMPDIFF(SECOND,stone_last_access,now()) * stone_rate) + stone_value AS stone," +
            "(TIMESTAMPDIFF(SECOND,iron_last_access,now()) * iron_rate) + iron_value AS iron," +
            "(TIMESTAMPDIFF(SECOND,copper_last_access,now()) * copper_rate) + copper_value AS copper," +
            "(TIMESTAMPDIFF(SECOND,silver_last_access,now()) * silver_rate) + silver_value AS silver," +
            "(TIMESTAMPDIFF(SECOND,gold_last_access,now()) * gold_rate) + gold_value AS gold" +
            " FROM user_cities WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {userno: userId}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}


function getResourcesFromCity(cityId, cb) {
    var sql = "SELECT " +
            "(TIMESTAMPDIFF(SECOND,food_last_access,now()) * food_rate) + food_value AS food,food_rate," +
            "(TIMESTAMPDIFF(SECOND,wood_last_access,now()) * wood_rate) + wood_value AS wood,wood_rate," +
            "(TIMESTAMPDIFF(SECOND,stone_last_access,now()) * stone_rate) + stone_value AS stone,stone_rate," +
            "(TIMESTAMPDIFF(SECOND,iron_last_access,now()) * iron_rate) + iron_value AS iron,iron_rate," +
            "(TIMESTAMPDIFF(SECOND,copper_last_access,now()) * copper_rate) + copper_value AS copper,copper_rate," +
            "(TIMESTAMPDIFF(SECOND,silver_last_access,now()) * silver_rate) + silver_value AS silver,silver_rate," +
            "(TIMESTAMPDIFF(SECOND,gold_last_access,now()) * gold_rate) + gold_value AS gold,gold_rate" +
            " FROM user_cities WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {cityno: cityId}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });

}

function reduceResourcesFromCity(cityId, resourceCost, cb) {
    var sql = "UPDATE user_cities SET " +
            "food_value= ((TIMESTAMPDIFF(SECOND,food_last_access,now()) * food_rate) + food_value) - " + Math.abs(resourceCost.food) + ",food_last_access=now(), " +
            "wood_value= ((TIMESTAMPDIFF(SECOND,wood_last_access,now()) * wood_rate) + wood_value) - " + Math.abs(resourceCost.wood) + ",wood_last_access=now(), " +
            "stone_value= ((TIMESTAMPDIFF(SECOND,stone_last_access,now()) * stone_rate) + stone_value) - " + Math.abs(resourceCost.stone) + ",stone_last_access=now(), " +
            "iron_value= ((TIMESTAMPDIFF(SECOND,iron_last_access,now()) * iron_rate) + iron_value) - " + Math.abs(resourceCost.iron) + ",iron_last_access=now(), " +
            "copper_value= ((TIMESTAMPDIFF(SECOND,copper_last_access,now()) * copper_rate) + copper_value) - " + Math.abs(resourceCost.copper) + ",copper_last_access=now(), " +
            "silver_value= ((TIMESTAMPDIFF(SECOND,silver_last_access,now()) * silver_rate) + silver_value) - " + Math.abs(resourceCost.silver) + ",silver_last_access=now(), " +
            "gold_value= ((TIMESTAMPDIFF(SECOND,gold_last_access,now()) * gold_rate) + gold_value) - " + Math.abs(resourceCost.gold) + ",gold_last_access=now() WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {cityno: cityId}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function increaseResourcesFromCity(cityId, resourceCost, cb) {
    var sql = "UPDATE user_cities SET " +
            "food_value= ((TIMESTAMPDIFF(SECOND,food_last_access,now()) * food_rate) + food_value) + " + Math.abs(resourceCost.food) + ",food_last_access=now(), " +
            "wood_value= ((TIMESTAMPDIFF(SECOND,wood_last_access,now()) * wood_rate) + wood_value) + " + Math.abs(resourceCost.wood) + ",wood_last_access=now(), " +
            "stone_value= ((TIMESTAMPDIFF(SECOND,stone_last_access,now()) * stone_rate) + stone_value) + " + Math.abs(resourceCost.stone) + ",stone_last_access=now(), " +
            "iron_value= ((TIMESTAMPDIFF(SECOND,iron_last_access,now()) * iron_rate) + iron_value) + " + Math.abs(resourceCost.iron) + ",iron_last_access=now(), " +
            "copper_value= ((TIMESTAMPDIFF(SECOND,copper_last_access,now()) * copper_rate) + copper_value) + " + Math.abs(resourceCost.copper) + ",copper_last_access=now(), " +
            "silver_value= ((TIMESTAMPDIFF(SECOND,silver_last_access,now()) * silver_rate) + silver_value) + " + Math.abs(resourceCost.silver) + ",silver_last_access=now(), " +
            "gold_value= ((TIMESTAMPDIFF(SECOND,gold_last_access,now()) * gold_rate) + gold_value) + " + Math.abs(resourceCost.gold) + ",gold_last_access=now() WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {cityno: cityId}, function (err, result) {
        console.log(query.sql);
        cb(err, result);
    });
}

function getXYOfTwoCity(cityId1, cityId2, callback) {
    var sql = "SELECT xpos, ypos FROM user_cities WHERE ? OR ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, [{cityno: cityId1}, {cityno: cityId2}], function (err, result) {
        callback(err, result);
    });
}

function changeOwnerOfCity(cityId, userId, callback) {
    var sql = "UPDATE user_cities SET ? WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, [{userno: userId, is_owned: 1}, {cityno: cityId}], function (err, result) {
        callback(err, result);
    });
}

function deleteAllUnitsFromCity(cityId, callback) {
    var sql = "DELETE FROM ron_city_units WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {cityno: cityId}, function (err, result) {
        callback(err, result);
    });
}

function downgradeToLevel1AllBuildings(cityId, callback) {

    var sql = "UPDATE `ron_city_buildings` SET ? WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, [{level: 1}, {city_id: cityId}], function (err, result) {
        callback(err, result);
    });
}

function getCityHappinessAndCourage(cityId, callback) {
    var sql = "SELECT happiness, courage FROM `user_cities` WHERE ?";

    //dbConnection.connect();
    var query = dbConnection.query(sql, {city_id: cityId}, function (err, result) {
        callback(err, result);
    });
}

module.exports.getCityHappinessAndCourage = getCityHappinessAndCourage;
module.exports.getXYOfTwoCity = getXYOfTwoCity;
module.exports.getAllCloseCity = getAllCloseCity;
module.exports.getUser = getUser;
module.exports.registrateUser = registrateUser;

module.exports.getAllUnitsByUser = getAllUnitsByUser;
module.exports.getAllUnitsFromCity = getAllUnitsFromCity;

module.exports.getAllBuildingsByUser = getAllBuildingsByUser;
module.exports.getAllBuildingsFromCity = getAllBuildingsFromCity;
module.exports.getUserCities = getUserCities;
module.exports.getResourcesFromCity = getResourcesFromCity;
module.exports.getAllFieldsByUser = getAllFieldsByUser;
module.exports.getAllFieldsFromCity = getAllFieldsFromCity;

module.exports.upgradeCityBuilding = upgradeCityBuilding;
module.exports.downgradeCityBuilding = downgradeCityBuilding;
module.exports.setCityBuildingStatus = setCityBuildingStatus;
module.exports.createCityBuilding = createCityBuilding;
module.exports.destroyCityBuilding = destroyCityBuilding;

module.exports.upgradeCityField = upgradeCityField;
module.exports.downgradeCityField = downgradeCityField;
module.exports.setCityFieldStatus = setCityFieldStatus;
module.exports.createCityField = createCityField;
module.exports.destroyCityField = destroyCityField;
module.exports.reduceResourcesFromCity = reduceResourcesFromCity;
module.exports.increaseResourcesFromCity = increaseResourcesFromCity;

module.exports.changeOwnerOfCity = changeOwnerOfCity;
module.exports.deleteAllUnitsFromCity = deleteAllUnitsFromCity;
module.exports.downgradeToLevel1AllBuildings = downgradeToLevel1AllBuildings;