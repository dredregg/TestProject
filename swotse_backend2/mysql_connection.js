var mysql = require('mysql');

var connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'newuser',
    password: 'password',
    database: 'swotse_node_db',
});

var mconnection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'swotse_db',
    multipleStatements: true,
});

function getConnection(){
    return connection;
}

function getMultipleConnection(){
    return mconnection;
}

module.exports.connection = getConnection;
module.exports.mconnection = getMultipleConnection;