var dbMatrix = require('./dbSqlFunctions/matrix');
var dbMarches = require('./dbSqlFunctions/marches');
var troops = require('./troops');


module.exports = function () {
    return {
        testTroopCollection: function (defenceTroops) {
            console.log(defenceTroops);
        },
        startScout: function (defenceTroops, attackTroops, march_id, afterSpyWar) {
            var defenceTroops = defenceTroops;
            var attackTroops = attackTroops;

            defenceTroops.initializeTroops();
            attackTroops.initializeTroops();

            var attackScoutRidderCount = attackTroops.getScoutRidderCount();
            var attackSpyCount = attackTroops.getSpyCount();
            var defenceScoutRidderCount = defenceTroops.getScoutRidderCount();
            var defenceSpyCount = defenceTroops.getSpyCount();

            if (attackScoutRidderCount > 0) {
                //with scout ridder
                var attackUnitSpy = attackScoutRidderCount + attackSpyCount;
                var defenceUnitSpy = defenceScoutRidderCount + defenceSpyCount;
                if (attackUnitSpy >= defenceUnitSpy) {
                    afterSpyWar(100, attackTroops, defenceTroops);//100 percent
                } else if (attackUnitSpy >= (defenceUnitSpy / 2)) {
                    afterSpyWar(50, attackTroops, defenceTroops);
                } else {
                    afterSpyWar(0, attackTroops, defenceTroops);
                }
            } else {
                //spy only
                //with scout ridder
                var attackUnitSpy = attackScoutRidderCount + attackSpyCount;
                var defenceUnitSpy = defenceScoutRidderCount + defenceSpyCount;
                if (attackUnitSpy >= defenceUnitSpy) {
                    afterSpyWar(100, attackTroops, defenceTroops);//100 percent
                } else if (attackUnitSpy >= (defenceUnitSpy / 2)) {
                    afterSpyWar(50, attackTroops, defenceTroops);
                } else {
                    afterSpyWar(0, attackTroops, defenceTroops);
                }
            }

        },
        startWar: function (defenceTroops, attackTroops, march_id, afterWar) {
//            var units2 = {
//                worker: unit(10, 100, 10, 20, 1, 'worker', 0),
//                spy: unit(10, 150, 20, 30, 1, 'spy', 1),
//                swords_man: unit(10, 250, 45, 70, 2, 'swords_man', 2),
//                spear_man: unit(10, 180, 50, 50, 6, 'spear_man', 3),
//                pike_man: unit(10, 220, 50, 50, 5, 'pike_man', 4),
//                scout_ridder: unit(10, 250, 50, 50, 2, 'scout_ridder', 5),
//                light_cavalry: unit(10, 300, 90, 85, 2, 'light_cavalry', 6),
//                heavy_cavalry: unit(10, 350, 150, 250, 4, 'heavy_cavalry', 7),
//                archer: unit(10, 180, 40, 50, 8, 'archer', 8),
//                archer_rider: unit(10, 300, 50, 90, 9, 'archer_rider', 9),
//                holly_man: unit(10, 500, 0, 0, 0, 'holy_man', 10),
//                wagon: unit(10, 100, 0, 0, 0, 'wagon', 11),
//                trebuchet: unit(10, 500, 450, 150, 14, 'trebuchet', 12),
//                siege_towers: unit(10, 1500, 300, 500, 2, 'siege_towers', 13),
//                battering_ram: unit(10, 1800, 450, 700, 1, 'battering_ram', 14),
//                ballista: unit(10, 450, 350, 150, 15, 'ballista', 15),
//            };

            var defenceTroops = defenceTroops;

            //defenceTroops.addTroops(dbMatrix.battleUnits[unitType], 1);
            //defenceTroops.addTroops(dbMatrix.battleUnits["holly_man"], 1);
//defenceTroops.showTroops();
            defenceTroops.initializeTroops();

// -- initialize the attack troop
            var attackTroops = attackTroops;

            //attackTroops.addTroops(dbMatrix.battleUnits[unitType], 10);
//attackTroops.addTroops(units["worker"], 1);
            attackTroops.initializeTroops();

            //defenceTroops.getAttack(1);

            var round = 15;
            while (attackTroops.canFight() && defenceTroops.canFight() && round > 0) {
                var attackValue = attackTroops.getAttack(round);
                if (attackValue > 0) {
                    console.log("--------------------------- attacker will attack -------------------------------------");
                    defenceTroops.absorbDamage(attackValue);
                }
                if (defenceTroops.canFight()) {
                    attackValue = defenceTroops.getAttack(round);
                    if (attackValue > 0) {
                        console.log("--------------------------- defender will attack -------------------------------------");
                        attackTroops.absorbDamage(attackValue);
                    }
                } else {
                    break;
                }
                if (!attackTroops.canFight()) {
                    break;
                }
                console.log('------------------------------------------------------------------------------- END OF ROUND:', round);
                round--;
            }

            if (attackTroops.canFight() && defenceTroops.canFight()) {
                console.log('------------------- TIE ------------------');
                dbMarches.insertMarchResult(march_id, 30);
                afterWar(30, attackTroops, defenceTroops);
            } else {
                if (attackTroops.canFight()) {
                    console.log('------------------- Attacker Won ------------------');
                    dbMarches.insertMarchResult(march_id, 10);
                    afterWar(10, attackTroops, defenceTroops);
                } else {
                    console.log('------------------- Defender Won ------------------');
                    dbMarches.insertMarchResult(march_id, 20);
                    afterWar(20, attackTroops, defenceTroops);
                }

            }
//            var x1 = 472;
//            var y1 = 467;
//
//            var x2 = 14;
//            var y2 = 406;
//            var a = Math.pow(x2 - x1, 2);
//            var b = Math.pow(y2 - y1, 2);
//            var distance = Math.sqrt(a + b);
//            var speed = 20;
//            console.log((distance * 100) / speed * 60);
        }
    };
};

