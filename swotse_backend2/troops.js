var __ = require('underscore');
module.exports = function () {
    return {
        troopsCollection: [],
        troopsAlive: [], //ordered by takeDamageOrder
        initializeTroops: function () {
            //whoo haaa!
            this.troopsAlive = __.sortBy(this.troopsCollection, function (troopBatch) {
                return troopBatch.unit.takeDamageOrder;
            });
            
            this.troopsAlive = JSON.parse(JSON.stringify(this.troopsAlive));
            console.log(this.troopsAlive);
            console.log("number of sacrificial lamb:", this.troopsAlive.length);
        },
        addTroops: function (unitObj, numberOfUnits) {
            this.troopsCollection.push({unit: unitObj, unitCount: numberOfUnits, unitsLife: (unitObj.life + unitObj.defence) * numberOfUnits});
        },
        showTroops: function () {
            console.log(this.troopsCollection);
        },
        getAttack: function (round) {
            var attackValue = 0;
            for (var troopIndex = 0; this.troopsAlive.length > troopIndex; troopIndex++) {
                //check the range of the troop if he can attack
                if (this.troopsAlive[troopIndex].unit.range >= round) {
                    attackValue += this.troopsAlive[troopIndex].unit.attack * this.troopsAlive[troopIndex].unitCount;
                }
            }
            console.log('AttackDamage:',attackValue);
            return attackValue;
        },
        
        absorbDamage: function (totalDamageToAbsorb) {
            for (var troopIndex = 0; this.troopsAlive.length > troopIndex; troopIndex++) {
                var troops = this.troopsAlive[troopIndex];
                if (totalDamageToAbsorb > 0) {
                    if (totalDamageToAbsorb >= troops.unitsLife) {
                        totalDamageToAbsorb -= troops.unitsLife;
                        troops.unitsLife = 0;
                        troops.unitCount = 0;
                        console.log('R.I.P. troop batch. -- pass the damage to next in line. boo hoo');
                    } else {
                        console.log('availableTroopsLife:',troops.unitsLife);
                        troops.unitsLife = troops.unitsLife - totalDamageToAbsorb;
                        troops.unitCount = Math.ceil(troops.unitsLife / (troops.unit.life + troops.unit.defence));
                        console.log('currentTroopsLife:',troops.unitsLife);
                        console.log('We can still fight! WHOOO HAAA!');
                        return 0;
                    }
                }else{
                    console.log('was here');
                    return 0;
                }
            }
            //this will return the remaining damage if the troops cant handle it anymore. BOOO!
            console.log('Troops Wiped Out. R.I.P.');
            return totalDamageToAbsorb;
        },
        canFight: function () {
            this.troopsAlive = __.reject(this.troopsAlive, function(troopBatch){
                return troopBatch.unitCount <= 0;
            });
            console.log('number of alive troops:',this.troopsAlive.length);
            return this.troopsAlive.length > 0;
        },
        getSpyCount: function(){
            var spyCount = 0;
            __.each(this.troopsCollection, function (element,index,list) {
                if(element.unit.name = "spy"){
                    spyCount += element.unitCount;
                } 
            });
            return spyCount;
        },
        getScoutRidderCount: function(){
            var scoutRidderCount = 0;
            __.each(this.troopsCollection, function (element,index,list) {
                if(element.unit.name = "scout_ridder"){
                    scoutRidderCount += element.unitCount;
                } 
            });
            return scoutRidderCount;
        },
    };

}


