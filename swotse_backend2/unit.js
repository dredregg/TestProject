module.exports = function (speed, health, attack, defence, range, type, takeDamageOrder, id, capacity, population) {
    return {
        speed: speed,
        attack: attack,
        life: health,
        defence: defence,
        range: range,
        name: type,
        id: id,
        takeDamageOrder: takeDamageOrder,
        capacity: capacity,
        population: population,
    }
}